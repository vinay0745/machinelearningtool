﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web;
using Microsoft.Win32;
using Microsoft.VisualBasic;

namespace MachineLearningTool
{
    public partial class Test : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // width of ellipse
            int nHeightEllipse // height of ellipse
        );


        public Test()
        {
            InitializeComponent();
            try
            {
                createinputcheckboxes();
                predictlabel.Text = "";
                // System.IO.File.Delete(Application.StartupPath + @"/currentPdfs/currentML_report.pdf");
               // Directory.Delete(Application.StartupPath + @"/currentPdfs");
               // createpredcheckboxes();
            }
            catch
            {
                this.Close();
                MessageBox.Show("No model has been created to predict");
            }

            try
            {
                System.IO.File.Delete(Application.StartupPath + @"/test_output.txt");
                System.IO.File.Delete(Application.StartupPath + @"/currentPdfs/currentML_report.pdf");
                Directory.Delete(Application.StartupPath + @"/currentPdfs");
            }
            catch
            {

            }
        }

        private void inpanel1_Paint(object sender, PaintEventArgs e)
        {
            System.IntPtr ptr = CreateRoundRectRgn(0, 0, inpanel1.Width, inpanel1.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
            inpanel1.Region = System.Drawing.Region.FromHrgn(ptr);
        }

        private void predpanel2_Paint(object sender, PaintEventArgs e)
        {
            System.IntPtr ptr = CreateRoundRectRgn(0, 0, predpanel2.Width, predpanel2.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
            predpanel2.Region = System.Drawing.Region.FromHrgn(ptr);
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            this.Close();
            try
            {
                System.IO.File.Delete(Application.StartupPath + @"/currentPdfs/currentML_report.pdf");
                Directory.Delete(Application.StartupPath + @"/currentPdfs");
                System.IO.File.Delete(Application.StartupPath + @"/test_output.txt");
            }
            catch
            {

            }
            

        }
        int i = 0;
        private void Predict_Click(object sender, EventArgs e)
        {
            try
            {
                System.IO.File.Delete(Application.StartupPath + @"/currentPdfs/currentML_report.pdf");
                Directory.Delete(Application.StartupPath + @"/currentPdfs");
            }
            catch
            {

            }
            for (int j = 0; j < MachineLearningTool.Form1.feat.Count; j++)
            {
                if(icheckboxlist[j].Text == string.Empty)
                {
                    predictlabel.Text = "";
                    MessageBox.Show("Put some input values");
                }
                else
                {
                    ProcessStartInfo start = new ProcessStartInfo();
                    start.WindowStyle = ProcessWindowStyle.Hidden;
                    start.CreateNoWindow = true;
                    start.UseShellExecute = false;
                    start.FileName = "cmd.exe";
                    for (int i = 0; i < MachineLearningTool.Form1.feat.Count; i++)
                    {

                        featureslabels.Add(icheckboxlist[i].Text);


                    }

                    string[] fileEntries = Directory.GetFiles(Application.StartupPath + "/", "*.pkl");
                    

                    if (fileEntries[0] == Application.StartupPath + "/LinearRegression.pkl")
                    {
                        test_OLS();
                      
                        start.Arguments = string.Format(@"/C " + MachineLearningTool.Form1.pypath +" LinearRegression_Test.py " + "-i " + String.Join(",", featureslabels));
                    }
                    else if (fileEntries[0] == Application.StartupPath + "/SGD.pkl")
                    {
                        test_SGD();
                        start.Arguments = string.Format(@"/C "+ MachineLearningTool.Form1.pypath + " SGD_Test.py " + "-i " + String.Join(",", featureslabels));
                    }
                    else if (fileEntries[0] == Application.StartupPath + "/DTR.pkl")
                    {
                        test_DTR();
                        start.Arguments = string.Format(@"/C "+ MachineLearningTool.Form1.pypath +" DTR_Test.py " + "-i " + String.Join(",", featureslabels));
                    }
                    else if (fileEntries[0] == Application.StartupPath + "/RFR.pkl")
                    {
                        test_RFR();
                        start.Arguments = string.Format(@"/C " + MachineLearningTool.Form1.pypath + " RFR_Test.py " + "-i " + String.Join(",", featureslabels));
                    }
                    else if (fileEntries[0] == Application.StartupPath + "/Perceptron.pkl")
                    {
                        test_perceptron();
                        start.Arguments = string.Format(@"/C " + MachineLearningTool.Form1.pypath + " Perceptron_Test.py " + "-i " + String.Join(",", featureslabels));
                    }
                    else if (fileEntries[0] == Application.StartupPath + "/DTC.pkl")
                    {
                        test_DTC();
                        start.Arguments = string.Format(@"/C " + MachineLearningTool.Form1.pypath + " DTC_Test.py " + "-i " + String.Join(",", featureslabels));
                    }
                    else if (fileEntries[0] == Application.StartupPath + "/RFC.pkl")
                    {
                        test_RFC();
                        start.Arguments = string.Format(@"/C " + MachineLearningTool.Form1.pypath + " RFC_Test.py " + "-i " + String.Join(",", featureslabels));
                    }

                    start.RedirectStandardOutput = true;

                    using (Process process = Process.Start(start))
                    {
                        using (StreamReader reader = process.StandardOutput)
                        {
                            string result = reader.ReadToEnd();
                            Console.Write(result);
                            predictlabel.Font = new System.Drawing.Font("Segoe UI", 15.0F, FontStyle.Bold);
                            predictlabel.Text = result;
                            using (StreamWriter sw = File.AppendText(Application.StartupPath + @"\test_output.txt"))
                            {
                                sw.WriteLine("Test "+ i);
                                sw.WriteLine("Inputs: "+String.Join(",", featureslabels));
                                sw.WriteLine("Prediction: "+ predictlabel.Text);

                                i += 1;
                            }
                        }
                    }
                    Console.WriteLine("********Predicted*******");
                    Console.WriteLine(fileEntries[0]);
                    featureslabels.Clear();
                    try
                    {
                        System.IO.File.Delete(Application.StartupPath + @"/LinearRegression_Test" + ".py");
                        System.IO.File.Delete(Application.StartupPath + @"/SGD_Test" + ".py");
                        System.IO.File.Delete(Application.StartupPath + @"/DTR_Test" + ".py");
                        System.IO.File.Delete(Application.StartupPath + @"/RFR_Test" + ".py");
                        System.IO.File.Delete(Application.StartupPath + @"/Perceptron_Test" + ".py");
                        System.IO.File.Delete(Application.StartupPath + @"/DTC_Test" + ".py");
                        System.IO.File.Delete(Application.StartupPath + @"/RFC_Test" + ".py");
                        //System.IO.File.Delete(Application.StartupPath + @"/RFC_Test" + ".py");
                    }
                    catch
                    {
                        Console.WriteLine("exception while deleting test script");
                    }
                    break;
                }
            }
            
        }

        private void Test_Load(object sender, EventArgs e)
        {
            System.IntPtr ptr = CreateRoundRectRgn(0, 0, this.Width, this.Height, 30, 30);
            this.Region = System.Drawing.Region.FromHrgn(ptr);
            for (int i = 0; i < MachineLearningTool.Form1.feat.Count; i++)
            {
                System.IntPtr ptr1 = CreateRoundRectRgn(0, 0, icheckboxlist[i].Width, icheckboxlist[i].Height, 10, 10); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
                icheckboxlist[i].Region = System.Drawing.Region.FromHrgn(ptr1);
            }
            
        }


        public List<string> featureslabels = new List<string>();
        public static TextBox o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, o18, o19, o20;

        private void savecurrentresult_Click(object sender, EventArgs e)
        {
            create_currentpdfreport();
            try
            {
                //predictlabel.Text = "";
                System.IO.File.Delete(Application.StartupPath + @"/currentPdfs/currentML_report.pdf");
                Directory.Delete(Application.StartupPath + @"/currentPdfs");
            }
            catch
            {

            }
            for (int j = 0; j < MachineLearningTool.Form1.feat.Count; j++)
            {
                if (icheckboxlist[j].Text == string.Empty)
                {
                    predictlabel.Text = "";
                    MessageBox.Show("Put some input values");
                }
                else
                {
                   
                    ProcessStartInfo start = new ProcessStartInfo();
                    start.WindowStyle = ProcessWindowStyle.Hidden;
                    start.CreateNoWindow = true;
                    start.UseShellExecute = false;
                    start.FileName = "cmd.exe";
                    start.Arguments = string.Format(@"/C " + MachineLearningTool.Form1.pypath + " " + Application.StartupPath + @"/currenttxttopdf.py");
                    start.RedirectStandardOutput = true;
                    using (Process process = Process.Start(start))
                    {
                        using (StreamReader reader = process.StandardOutput)
                        {
                            string pdf = reader.ReadToEnd();
                            Console.WriteLine(pdf);
                            MessageBox.Show("Pdf file generated and will be save on your Desktop Folder");
                        }
                    }

                    System.IO.File.Delete(Application.StartupPath + @"/currenttxttopdf.py");
                    System.IO.File.Delete(Application.StartupPath + @"/currentDocuments/ML.docx");
                    Directory.Delete(Application.StartupPath + @"/currentDocuments");
                    System.IO.File.Copy(Application.StartupPath + @"/currentPdfs/currentML_report.pdf", Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"/currentML_report.pdf", true);
                }
            }

        }



        public void create_currentpdfreport()
        {
            string default_currentpdfreport = @"from docx import Document
from docx.enum.text import WD_UNDERLINE
import time
import os
import sys
import comtypes.client
import glob


wordpath           = r'currentDocuments'
pdfpath            = r'currentPdfs'
outputdata = """"


txtfile = open(r""" + Application.StartupPath + @"/test_output.txt" + @""",'r')
outputdata = txtfile.read()
txtfile.close()


def ReplacePrefix(document, prefixdata, toreplace):
    for paragraph in document.paragraphs:
        if toreplace in paragraph.text:
            inline = paragraph.runs
            for i in range(len(inline)):
                if toreplace in inline[i].text:
                    text = inline[i].text.replace(toreplace, ""{0}"".format(prefixdata))
                    inline[i].text = text

os.mkdir(os.path.abspath(wordpath))
os.mkdir(os.path.abspath(pdfpath))

document = Document(r""" + Application.StartupPath + @"/Eduvance_ML_report.docx" + @""")

ReplacePrefix(document, outputdata, ""!!"")

temppath = r""./currentDocuments/ML.docx""
document.save(temppath)
time.sleep(2)

wdFormatPDF = 17
try:
    infile = (glob.glob(""%s\\*.docx"" % (wordpath)))
    word = comtypes.client.CreateObject('word.Application')
    for in_file in infile: 
        out_file = os.path.abspath(r""./currentPdfs/currentML_report.pdf"")
        doc = word.Documents.Open(os.path.abspath(in_file))
        doc.SaveAs(out_file, FileFormat = wdFormatPDF)
        doc.Close()
        print(""{0} file done"".format(in_file))
    word.Quit()
    time.sleep(2)
except Exception as e:
    print(e)
    doc.Close()
    word.Quit()";

            string from = Application.StartupPath + @"/currenttxttopdf.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_currentpdfreport);

            }
        }

        public static TextBox i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i26, i17, i18, i19, i20;
        public List<TextBox> icheckboxlist = new List<TextBox> { i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i26, i17, i18, i19, i20 };
        public List<TextBox> ocheckboxlist = new List<TextBox> { o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, o18, o19, o20 };
        public static Label l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15, l16, l17, l18, l19, l20;
        public List<Label> labellist = new List<Label> { l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15, l16, l17, l18, l19, l20 };

        public void createinputcheckboxes()
        {
            int ypos = 10;
           // int ypos1 = 40;
            
            inputpanels.HorizontalScroll.Maximum = 0;
            inputpanels.AutoScroll = false;
            inputpanels.VerticalScroll.Visible = false;
            inputpanels.AutoScroll = true;
            for (int i = 0; i < MachineLearningTool.Form1.feat.Count; i++)
            {
               

                for (int j = 0; j < MachineLearningTool.Form1.feat.Count; j++)
                {
                    labellist[j] = new Label();
                    labellist[j].Location = new Point(60, ypos);
                    labellist[j].Text = MachineLearningTool.Form1.feat[i];
                    labellist[j].Size = new System.Drawing.Size(300, 22);
                    labellist[j].Font = new System.Drawing.Font("Segoe UI", 12.0F, FontStyle.Regular);
                    labellist[j].ForeColor = Color.Black;

                }


                icheckboxlist[i] = new TextBox();
                icheckboxlist[i].Location = new Point(240, ypos);
                icheckboxlist[i].BorderStyle = BorderStyle.None;
                icheckboxlist[i].Size = new System.Drawing.Size(60, 50);
                icheckboxlist[i].Text = String.Empty;
                icheckboxlist[i].TabStop = false;
                icheckboxlist[i].Font = new System.Drawing.Font("Segoe UI", 12.0F, FontStyle.Regular);
                icheckboxlist[i].TextAlign = HorizontalAlignment.Center;


                inputpanels.Controls.Add(icheckboxlist[i]);
                inputpanels.Controls.Add(labellist[i]);

                ypos += 30;
                //ypos1 += 60;
                //icheckboxlist[i].OnChange += new EventHandler(chkcheckboxes);
            }



        }

            public void test_OLS()
            {
                string default_testols = @"import pickle
import sys
import getopt
import numpy as np
pkl_filename = ""LinearRegression.pkl""
with open(pkl_filename, 'rb') as file:
    pickle_model = pickle.load(file)


def main(argv):
    newinputvalues = []
    try:
        opts, args = getopt.getopt(argv, ""i:"",[""ifile=""])
    except getopt.GetoptError:
        print(""Pythonfile.py -i <inputvalues>"")
        sys.exit(2)
    for opt, arg in opts:
        if opt in (""-i"", ""--ifile""):
            inputvalue = arg
        else:
            print(""Pythonfile.py -i <inputvalues>"")
            sys.exit()
    try:
        value = float(inputvalue)
    except:
            for i in inputvalue.split("",""):

                newinputvalues.append(float(i))
    
            Xtest = [newinputvalues] 
    
            Ypredict = pickle_model.predict(Xtest)
    
            print(Ypredict[0][0])
    else:
        Xtest = [[value]]
        Ypredict = pickle_model.predict(Xtest)
        print(Ypredict[0][0])


if __name__ == ""__main__"":
    main(sys.argv[1:])
";

                string from = Application.StartupPath + "/LinearRegression_Test.py";
                using (StreamWriter file = new StreamWriter(from))
                {

                    file.WriteLine(default_testols);

                }

            }

        public void test_SGD()
        {
            string default_testsgd = @"import pickle
import sys
import getopt
import numpy as np
pkl_filename = ""SGD.pkl""
with open(pkl_filename, 'rb') as file:
    pickle_model = pickle.load(file)


def main(argv):
    newinputvalues = []
    try:
        opts, args = getopt.getopt(argv, ""i:"",[""ifile=""])
    except getopt.GetoptError:
        print(""Pythonfile.py -i <inputvalues>"")
        sys.exit(2)
    for opt, arg in opts:
        if opt in (""-i"", ""--ifile""):
            inputvalue = arg
        else:
            print(""Pythonfile.py -i <inputvalues>"")
            sys.exit()
    try:
        value = float(inputvalue)
    except:
            for i in inputvalue.split("",""):

                newinputvalues.append(float(i))
    
            Xtest = [newinputvalues]
    
            Ypredict = pickle_model.predict(Xtest)
    
            print(Ypredict[0])
    else:
        Xtest = [[value]]
        Ypredict = pickle_model.predict(Xtest)
        print(Ypredict[0])


if __name__ == ""__main__"":
    main(sys.argv[1:])
";
            string from = Application.StartupPath + "/SGD_Test.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_testsgd);

            }
        }

        public void test_DTR()
        {
            String default_testDTR = @"import pickle
import sys
import getopt
import numpy as np
pkl_filename = ""DTR.pkl""
with open(pkl_filename, 'rb') as file:
    pickle_model = pickle.load(file)


def main(argv):
    newinputvalues = []
    try:
        opts, args = getopt.getopt(argv, ""i:"",[""ifile=""])
    except getopt.GetoptError:
        print(""Pythonfile.py -i <inputvalues>"")
        sys.exit(2)
    for opt, arg in opts:
        if opt in (""-i"", ""--ifile""):
            inputvalue = arg
        else:
            print(""Pythonfile.py -i <inputvalues>"")
            sys.exit()
    try:
        value = float(inputvalue)
    except:
            for i in inputvalue.split("",""):

                newinputvalues.append(float(i))
    
            Xtest = [newinputvalues]
    
            Ypredict = pickle_model.predict(Xtest)
    
            print(Ypredict[0])
    else:
        Xtest = [[value]]
        Ypredict = pickle_model.predict(Xtest)
        print(Ypredict[0])


if __name__ == ""__main__"":
    main(sys.argv[1:])";

            string from = Application.StartupPath + "/DTR_Test.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_testDTR);

            }
        }

        public void test_RFR()
        {
            string default_testRFR = @"import pickle
import sys
import getopt
import numpy as np
pkl_filename = ""RFR.pkl""
with open(pkl_filename, 'rb') as file:
    pickle_model = pickle.load(file)


def main(argv):
    newinputvalues = []
    try:
        opts, args = getopt.getopt(argv, ""i:"",[""ifile=""])
    except getopt.GetoptError:
        print(""Pythonfile.py -i <inputvalues>"")
        sys.exit(2)
    for opt, arg in opts:
        if opt in (""-i"", ""--ifile""):
            inputvalue = arg
        else:
            print(""Pythonfile.py -i <inputvalues>"")
            sys.exit()
    try:
        value = float(inputvalue)
    except:
            for i in inputvalue.split("",""):

                newinputvalues.append(float(i))
    
            Xtest = [newinputvalues]
    
            Ypredict = pickle_model.predict(Xtest)
    
            print(Ypredict[0])
    else:
        Xtest = [[value]]
        Ypredict = pickle_model.predict(Xtest)
        print(Ypredict[0])


if __name__ == ""__main__"":
    main(sys.argv[1:])
";

            string from = Application.StartupPath + "/RFR_Test.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_testRFR);

            }

        }

        public void test_perceptron()
        {
            string default_testpercep = @"import pickle
import sys
import getopt
import numpy as np
pkl_filename = ""Perceptron.pkl""
with open(pkl_filename, 'rb') as file:
    pickle_model = pickle.load(file)


def main(argv):
    newinputvalues = []
    try:
        opts, args = getopt.getopt(argv, ""i:"",[""ifile=""])
    except getopt.GetoptError:
        print(""Pythonfile.py -i <inputvalues>"")
        sys.exit(2)
    for opt, arg in opts:
        if opt in (""-i"", ""--ifile""):
            inputvalue = arg
        else:
            print(""Pythonfile.py -i <inputvalues>"")
            sys.exit()
    try:
        value = float(inputvalue)
    except:
            for i in inputvalue.split("",""):

                newinputvalues.append(float(i))
    
            Xtest = [newinputvalues]
    
            Ypredict = pickle_model.predict(Xtest)
    
            print(Ypredict[0])
    else:
        Xtest = [[value]]
        Ypredict = pickle_model.predict(Xtest)
        print(Ypredict[0])


if __name__ == ""__main__"":
    main(sys.argv[1:])
";


            string from = Application.StartupPath + "/Perceptron_Test.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_testpercep);

            }
        }

        public void test_DTC()
        {
            string default_DTC = @"import pickle
import sys
import getopt
import numpy as np
pkl_filename = ""DTC.pkl""
with open(pkl_filename, 'rb') as file:
    pickle_model = pickle.load(file)


def main(argv):
    newinputvalues = []
    try:
        opts, args = getopt.getopt(argv, ""i:"",[""ifile=""])
    except getopt.GetoptError:
        print(""Pythonfile.py -i <inputvalues>"")
        sys.exit(2)
    for opt, arg in opts:
        if opt in (""-i"", ""--ifile""):
            inputvalue = arg
        else:
            print(""Pythonfile.py -i <inputvalues>"")
            sys.exit()
    try:
        value = float(inputvalue)
    except:
            for i in inputvalue.split("",""):

                newinputvalues.append(float(i))
    
            Xtest = [newinputvalues]
    
            Ypredict = pickle_model.predict(Xtest)
    
            print(Ypredict[0])
    else:
        Xtest = [[value]]
        Ypredict = pickle_model.predict(Xtest)
        print(Ypredict[0])


if __name__ == ""__main__"":
    main(sys.argv[1:])
";

            string from = Application.StartupPath + "/DTC_Test.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_DTC);

            }
        }

        public void test_RFC()
        {
            string default_RFC = @"import pickle
import sys
import getopt
import numpy as np
pkl_filename = ""RFC.pkl""
with open(pkl_filename, 'rb') as file:
    pickle_model = pickle.load(file)


def main(argv):
    newinputvalues = []
    try:
        opts, args = getopt.getopt(argv, ""i:"",[""ifile=""])
    except getopt.GetoptError:
        print(""Pythonfile.py -i <inputvalues>"")
        sys.exit(2)
    for opt, arg in opts:
        if opt in (""-i"", ""--ifile""):
            inputvalue = arg
        else:
            print(""Pythonfile.py -i <inputvalues>"")
            sys.exit()
    try:
        value = float(inputvalue)
    except:
            for i in inputvalue.split("",""):

                newinputvalues.append(float(i))
    
            Xtest = [newinputvalues]
    
            Ypredict = pickle_model.predict(Xtest)
    
            print(Ypredict[0])
    else:
        Xtest = [[value]]
        Ypredict = pickle_model.predict(Xtest)
        print(Ypredict[0])


if __name__ == ""__main__"":
    main(sys.argv[1:])
";

            string from = Application.StartupPath + "/RFC_Test.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_RFC);

            }
        }
    }
}
