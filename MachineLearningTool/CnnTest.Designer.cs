﻿namespace MachineLearningTool
{
    partial class CnnTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CnnTest));
            this.inpanel1 = new System.Windows.Forms.Panel();
            this.Cnntestpicture = new System.Windows.Forms.PictureBox();
            this.Browsebutton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuThinButton21 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.Predict = new Bunifu.Framework.UI.BunifuThinButton2();
            this.predpanel2 = new System.Windows.Forms.Panel();
            this.predictionpanels = new System.Windows.Forms.Panel();
            this.predictlabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.inpanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Cnntestpicture)).BeginInit();
            this.predpanel2.SuspendLayout();
            this.predictionpanels.SuspendLayout();
            this.SuspendLayout();
            // 
            // inpanel1
            // 
            this.inpanel1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.inpanel1.Controls.Add(this.Cnntestpicture);
            this.inpanel1.Controls.Add(this.Browsebutton);
            this.inpanel1.Controls.Add(this.bunifuCustomLabel1);
            this.inpanel1.Location = new System.Drawing.Point(285, 75);
            this.inpanel1.Name = "inpanel1";
            this.inpanel1.Size = new System.Drawing.Size(569, 362);
            this.inpanel1.TabIndex = 3;
            this.inpanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.inpanel1_Paint);
            // 
            // Cnntestpicture
            // 
            this.Cnntestpicture.Location = new System.Drawing.Point(38, 131);
            this.Cnntestpicture.Name = "Cnntestpicture";
            this.Cnntestpicture.Size = new System.Drawing.Size(507, 217);
            this.Cnntestpicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Cnntestpicture.TabIndex = 22;
            this.Cnntestpicture.TabStop = false;
            // 
            // Browsebutton
            // 
            this.Browsebutton.ActiveBorderThickness = 1;
            this.Browsebutton.ActiveCornerRadius = 20;
            this.Browsebutton.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.Browsebutton.ActiveForecolor = System.Drawing.Color.White;
            this.Browsebutton.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.Browsebutton.BackColor = System.Drawing.Color.LightSteelBlue;
            this.Browsebutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Browsebutton.BackgroundImage")));
            this.Browsebutton.ButtonText = "Select File";
            this.Browsebutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Browsebutton.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Browsebutton.ForeColor = System.Drawing.Color.Black;
            this.Browsebutton.IdleBorderThickness = 1;
            this.Browsebutton.IdleCornerRadius = 20;
            this.Browsebutton.IdleFillColor = System.Drawing.Color.White;
            this.Browsebutton.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.Browsebutton.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.Browsebutton.Location = new System.Drawing.Point(112, 52);
            this.Browsebutton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Browsebutton.Name = "Browsebutton";
            this.Browsebutton.Size = new System.Drawing.Size(360, 58);
            this.Browsebutton.TabIndex = 21;
            this.Browsebutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Browsebutton.Click += new System.EventHandler(this.Browsebutton_Click);
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(255, 9);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(81, 38);
            this.bunifuCustomLabel1.TabIndex = 0;
            this.bunifuCustomLabel1.Text = "Input";
            this.bunifuCustomLabel1.Click += new System.EventHandler(this.bunifuCustomLabel1_Click);
            // 
            // bunifuThinButton21
            // 
            this.bunifuThinButton21.ActiveBorderThickness = 1;
            this.bunifuThinButton21.ActiveCornerRadius = 40;
            this.bunifuThinButton21.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton21.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bunifuThinButton21.BackColor = System.Drawing.Color.Gainsboro;
            this.bunifuThinButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton21.BackgroundImage")));
            this.bunifuThinButton21.ButtonText = "Back";
            this.bunifuThinButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton21.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton21.ForeColor = System.Drawing.Color.Black;
            this.bunifuThinButton21.IdleBorderThickness = 2;
            this.bunifuThinButton21.IdleCornerRadius = 40;
            this.bunifuThinButton21.IdleFillColor = System.Drawing.Color.White;
            this.bunifuThinButton21.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.Location = new System.Drawing.Point(587, 570);
            this.bunifuThinButton21.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.bunifuThinButton21.Name = "bunifuThinButton21";
            this.bunifuThinButton21.Size = new System.Drawing.Size(184, 73);
            this.bunifuThinButton21.TabIndex = 8;
            this.bunifuThinButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton21.Click += new System.EventHandler(this.bunifuThinButton21_Click);
            // 
            // Predict
            // 
            this.Predict.ActiveBorderThickness = 1;
            this.Predict.ActiveCornerRadius = 40;
            this.Predict.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.Predict.ActiveForecolor = System.Drawing.Color.White;
            this.Predict.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.Predict.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Predict.BackColor = System.Drawing.Color.Gainsboro;
            this.Predict.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Predict.BackgroundImage")));
            this.Predict.ButtonText = "Predict";
            this.Predict.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Predict.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Predict.ForeColor = System.Drawing.Color.Black;
            this.Predict.IdleBorderThickness = 2;
            this.Predict.IdleCornerRadius = 40;
            this.Predict.IdleFillColor = System.Drawing.Color.White;
            this.Predict.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.Predict.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.Predict.Location = new System.Drawing.Point(381, 571);
            this.Predict.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Predict.Name = "Predict";
            this.Predict.Size = new System.Drawing.Size(184, 73);
            this.Predict.TabIndex = 7;
            this.Predict.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Predict.Click += new System.EventHandler(this.Predict_Click);
            // 
            // predpanel2
            // 
            this.predpanel2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.predpanel2.Controls.Add(this.predictionpanels);
            this.predpanel2.Controls.Add(this.bunifuCustomLabel2);
            this.predpanel2.Location = new System.Drawing.Point(285, 452);
            this.predpanel2.Name = "predpanel2";
            this.predpanel2.Size = new System.Drawing.Size(569, 104);
            this.predpanel2.TabIndex = 6;
            this.predpanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.predpanel2_Paint);
            // 
            // predictionpanels
            // 
            this.predictionpanels.Controls.Add(this.predictlabel);
            this.predictionpanels.Location = new System.Drawing.Point(150, 3);
            this.predictionpanels.Name = "predictionpanels";
            this.predictionpanels.Size = new System.Drawing.Size(416, 80);
            this.predictionpanels.TabIndex = 2;
            // 
            // predictlabel
            // 
            this.predictlabel.AutoSize = true;
            this.predictlabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.predictlabel.Location = new System.Drawing.Point(127, 28);
            this.predictlabel.Name = "predictlabel";
            this.predictlabel.Size = new System.Drawing.Size(24, 28);
            this.predictlabel.TabIndex = 3;
            this.predictlabel.Text = "o";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(15, 27);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(142, 38);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "Prediction";
            // 
            // CnnTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1148, 720);
            this.Controls.Add(this.bunifuThinButton21);
            this.Controls.Add(this.Predict);
            this.Controls.Add(this.predpanel2);
            this.Controls.Add(this.inpanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "CnnTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CnnTest";
            this.Load += new System.EventHandler(this.CnnTest_Load);
            this.inpanel1.ResumeLayout(false);
            this.inpanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Cnntestpicture)).EndInit();
            this.predpanel2.ResumeLayout(false);
            this.predpanel2.PerformLayout();
            this.predictionpanels.ResumeLayout(false);
            this.predictionpanels.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel inpanel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuThinButton2 Browsebutton;
        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton21;
        private Bunifu.Framework.UI.BunifuThinButton2 Predict;
        private System.Windows.Forms.Panel predpanel2;
        private System.Windows.Forms.Panel predictionpanels;
        private Bunifu.Framework.UI.BunifuCustomLabel predictlabel;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private System.Windows.Forms.PictureBox Cnntestpicture;
    }
}