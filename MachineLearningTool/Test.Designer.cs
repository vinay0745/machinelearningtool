﻿namespace MachineLearningTool
{
    partial class Test
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Test));
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.inpanel1 = new System.Windows.Forms.Panel();
            this.inputpanels = new System.Windows.Forms.Panel();
            this.predpanel2 = new System.Windows.Forms.Panel();
            this.predictionpanels = new System.Windows.Forms.Panel();
            this.predictlabel = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.Predict = new Bunifu.Framework.UI.BunifuThinButton2();
            this.bunifuThinButton21 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.savecurrentresult = new Bunifu.Framework.UI.BunifuThinButton2();
            this.inpanel1.SuspendLayout();
            this.predpanel2.SuspendLayout();
            this.predictionpanels.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(240, 12);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(81, 38);
            this.bunifuCustomLabel1.TabIndex = 0;
            this.bunifuCustomLabel1.Text = "Input";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(12, 27);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(142, 38);
            this.bunifuCustomLabel2.TabIndex = 1;
            this.bunifuCustomLabel2.Text = "Prediction";
            // 
            // inpanel1
            // 
            this.inpanel1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.inpanel1.Controls.Add(this.inputpanels);
            this.inpanel1.Controls.Add(this.bunifuCustomLabel1);
            this.inpanel1.Location = new System.Drawing.Point(279, 82);
            this.inpanel1.Name = "inpanel1";
            this.inpanel1.Size = new System.Drawing.Size(569, 362);
            this.inpanel1.TabIndex = 2;
            this.inpanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.inpanel1_Paint);
            // 
            // inputpanels
            // 
            this.inputpanels.Location = new System.Drawing.Point(29, 70);
            this.inputpanels.Name = "inputpanels";
            this.inputpanels.Size = new System.Drawing.Size(507, 274);
            this.inputpanels.TabIndex = 1;
            // 
            // predpanel2
            // 
            this.predpanel2.BackColor = System.Drawing.Color.LightSteelBlue;
            this.predpanel2.Controls.Add(this.predictionpanels);
            this.predpanel2.Controls.Add(this.bunifuCustomLabel2);
            this.predpanel2.Location = new System.Drawing.Point(279, 462);
            this.predpanel2.Name = "predpanel2";
            this.predpanel2.Size = new System.Drawing.Size(569, 104);
            this.predpanel2.TabIndex = 3;
            this.predpanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.predpanel2_Paint);
            // 
            // predictionpanels
            // 
            this.predictionpanels.Controls.Add(this.predictlabel);
            this.predictionpanels.Location = new System.Drawing.Point(150, 3);
            this.predictionpanels.Name = "predictionpanels";
            this.predictionpanels.Size = new System.Drawing.Size(416, 80);
            this.predictionpanels.TabIndex = 2;
            // 
            // predictlabel
            // 
            this.predictlabel.AutoSize = true;
            this.predictlabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.predictlabel.Location = new System.Drawing.Point(118, 24);
            this.predictlabel.Name = "predictlabel";
            this.predictlabel.Size = new System.Drawing.Size(24, 28);
            this.predictlabel.TabIndex = 3;
            this.predictlabel.Text = "o";
            // 
            // Predict
            // 
            this.Predict.ActiveBorderThickness = 1;
            this.Predict.ActiveCornerRadius = 40;
            this.Predict.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.Predict.ActiveForecolor = System.Drawing.Color.White;
            this.Predict.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.Predict.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Predict.BackColor = System.Drawing.Color.Gainsboro;
            this.Predict.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Predict.BackgroundImage")));
            this.Predict.ButtonText = "Predict";
            this.Predict.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Predict.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Predict.ForeColor = System.Drawing.Color.Black;
            this.Predict.IdleBorderThickness = 2;
            this.Predict.IdleCornerRadius = 40;
            this.Predict.IdleFillColor = System.Drawing.Color.White;
            this.Predict.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.Predict.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.Predict.Location = new System.Drawing.Point(279, 578);
            this.Predict.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.Predict.Name = "Predict";
            this.Predict.Size = new System.Drawing.Size(184, 87);
            this.Predict.TabIndex = 4;
            this.Predict.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Predict.Click += new System.EventHandler(this.Predict_Click);
            // 
            // bunifuThinButton21
            // 
            this.bunifuThinButton21.ActiveBorderThickness = 1;
            this.bunifuThinButton21.ActiveCornerRadius = 40;
            this.bunifuThinButton21.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton21.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bunifuThinButton21.BackColor = System.Drawing.Color.Gainsboro;
            this.bunifuThinButton21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton21.BackgroundImage")));
            this.bunifuThinButton21.ButtonText = "Back";
            this.bunifuThinButton21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton21.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton21.ForeColor = System.Drawing.Color.Black;
            this.bunifuThinButton21.IdleBorderThickness = 2;
            this.bunifuThinButton21.IdleCornerRadius = 40;
            this.bunifuThinButton21.IdleFillColor = System.Drawing.Color.White;
            this.bunifuThinButton21.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton21.Location = new System.Drawing.Point(695, 578);
            this.bunifuThinButton21.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.bunifuThinButton21.Name = "bunifuThinButton21";
            this.bunifuThinButton21.Size = new System.Drawing.Size(184, 87);
            this.bunifuThinButton21.TabIndex = 5;
            this.bunifuThinButton21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton21.Click += new System.EventHandler(this.bunifuThinButton21_Click);
            // 
            // savecurrentresult
            // 
            this.savecurrentresult.ActiveBorderThickness = 1;
            this.savecurrentresult.ActiveCornerRadius = 40;
            this.savecurrentresult.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.savecurrentresult.ActiveForecolor = System.Drawing.Color.White;
            this.savecurrentresult.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.savecurrentresult.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.savecurrentresult.BackColor = System.Drawing.Color.Gainsboro;
            this.savecurrentresult.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("savecurrentresult.BackgroundImage")));
            this.savecurrentresult.ButtonText = "Save Current Result";
            this.savecurrentresult.Cursor = System.Windows.Forms.Cursors.Hand;
            this.savecurrentresult.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savecurrentresult.ForeColor = System.Drawing.Color.Black;
            this.savecurrentresult.IdleBorderThickness = 2;
            this.savecurrentresult.IdleCornerRadius = 40;
            this.savecurrentresult.IdleFillColor = System.Drawing.Color.White;
            this.savecurrentresult.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.savecurrentresult.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.savecurrentresult.Location = new System.Drawing.Point(473, 578);
            this.savecurrentresult.Margin = new System.Windows.Forms.Padding(4);
            this.savecurrentresult.Name = "savecurrentresult";
            this.savecurrentresult.Size = new System.Drawing.Size(212, 87);
            this.savecurrentresult.TabIndex = 6;
            this.savecurrentresult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.savecurrentresult.Click += new System.EventHandler(this.savecurrentresult_Click);
            // 
            // Test
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1149, 720);
            this.Controls.Add(this.savecurrentresult);
            this.Controls.Add(this.bunifuThinButton21);
            this.Controls.Add(this.Predict);
            this.Controls.Add(this.predpanel2);
            this.Controls.Add(this.inpanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Test";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Test";
            this.Load += new System.EventHandler(this.Test_Load);
            this.inpanel1.ResumeLayout(false);
            this.inpanel1.PerformLayout();
            this.predpanel2.ResumeLayout(false);
            this.predpanel2.PerformLayout();
            this.predictionpanels.ResumeLayout(false);
            this.predictionpanels.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        private System.Windows.Forms.Panel inpanel1;
        private System.Windows.Forms.Panel predpanel2;
        private System.Windows.Forms.Panel inputpanels;
        private System.Windows.Forms.Panel predictionpanels;
        private Bunifu.Framework.UI.BunifuThinButton2 Predict;
        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton21;
        private Bunifu.Framework.UI.BunifuCustomLabel predictlabel;
        private Bunifu.Framework.UI.BunifuThinButton2 savecurrentresult;
    }
}