﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web;
using Microsoft.Win32;
using Microsoft.VisualBasic;

namespace MachineLearningTool
{
    public partial class CnnTest : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
       (
           int nLeftRect,     // x-coordinate of upper-left corner
           int nTopRect,      // y-coordinate of upper-left corner
           int nRightRect,    // x-coordinate of lower-right corner
           int nBottomRect,   // y-coordinate of lower-right corner
           int nWidthEllipse, // width of ellipse
           int nHeightEllipse // height of ellipse
       );

        public CnnTest()
        {
            InitializeComponent();
            predictlabel.Text = "";
        }

        private void inpanel1_Paint(object sender, PaintEventArgs e)
        {
            System.IntPtr ptr = CreateRoundRectRgn(0, 0, inpanel1.Width, inpanel1.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
            inpanel1.Region = System.Drawing.Region.FromHrgn(ptr);
        }

        private void predpanel2_Paint(object sender, PaintEventArgs e)
        {
            System.IntPtr ptr = CreateRoundRectRgn(0, 0, predpanel2.Width, predpanel2.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
            predpanel2.Region = System.Drawing.Region.FromHrgn(ptr);
        }

        private void CnnTest_Load(object sender, EventArgs e)
        {
            System.IntPtr ptr = CreateRoundRectRgn(0, 0, this.Width, this.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
            this.Region = System.Drawing.Region.FromHrgn(ptr);
        }

        public string datafilename;
        public string filename;
        private void Browsebutton_Click(object sender, EventArgs e)
        {
            predictlabel.Text = "";
            System.Windows.Forms.OpenFileDialog openfiledialog = new System.Windows.Forms.OpenFileDialog();
            openfiledialog.Filter = "JPG|*.jpg;*.jpeg|PNG|*.png";
            
            //Console.WriteLine(filename);
            

            if (openfiledialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {

                datafilename = openfiledialog.FileName;
                filename = openfiledialog.SafeFileName;
                Console.WriteLine(filename);
                System.IO.File.Copy(datafilename, Application.StartupPath + @"\" + filename, true);
                Cnntestpicture.ImageLocation = Application.StartupPath + @"\" + filename;

            }

        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Predict_Click(object sender, EventArgs e)
        {
            if(datafilename == string.Empty)
            {
                MessageBox.Show("Select Input image");
                predictlabel.Text = "";
                datafilename = "";
            }
            else
            {

                predictlabel.Text = "";
                //System.IO.File.Copy(datafilename, Application.StartupPath + @"\test\" + filename, true);
                ProcessStartInfo start = new ProcessStartInfo();
                start.WindowStyle = ProcessWindowStyle.Hidden;
                start.CreateNoWindow = true;
                start.UseShellExecute = false;
                start.FileName = "cmd.exe";
                string[] fileEntries = Directory.GetFiles(Application.StartupPath + "/", "*.h5");
                Console.WriteLine("%%%%%%%");
                Console.WriteLine(fileEntries[0]);
                if (fileEntries[0] == Application.StartupPath + "/cnn.h5")
                {
                    test_cnn();
                    Console.WriteLine(fileEntries[0]);
                    start.Arguments = string.Format(@"/C " + MachineLearningTool.Form1.pypath + " IC_Test.py");
                }

                start.RedirectStandardOutput = true;

                using (Process process = Process.Start(start))
                {
                    using (StreamReader reader = process.StandardOutput)
                    {
                        string result = reader.ReadToEnd();
                        Console.Write(result);
                        predictlabel.Font = new System.Drawing.Font("Segoe UI", 15.0F, FontStyle.Bold);
                        predictlabel.Text = result;
                    }
                }
                Console.WriteLine("********Predicted*******");
                Console.WriteLine(fileEntries[0]);
                try
                {
                    System.IO.File.Delete(Application.StartupPath + @"/IC_Test" + ".py");
                    string[] pathproject3 = Directory.GetFiles(Application.StartupPath, "*.jpg");
                    foreach (string filename in pathproject3)
                    {
                        System.IO.File.Delete(filename);

                    }
                    string[] pathproject2 = Directory.GetFiles(Application.StartupPath, "*.png");
                    foreach (string filename in pathproject2)
                    {
                        System.IO.File.Delete(filename);

                    }
                    datafilename = "";
                }
                catch
                {
                    Console.WriteLine("exception while deleting test script");
                }

            }
            

        }

        public void test_cnn()
        {
            string default_testcnn = @"import pickle
from keras.models import load_model
from keras.preprocessing import image
import numpy as np
from keras.models import model_from_json
import os
with open('cnn.json', 'r') as f:
    model = model_from_json(f.read())

model.load_weights('cnn.h5')

img_width, img_height = 128, 128
img = image.load_img(r""" + Application.StartupPath + @"\" + filename + @""", target_size = (img_width, img_height))
img = image.img_to_array(img)
img = np.expand_dims(img, axis = 0)

list1 =[]
for root, dirs, files in os.walk(r""" + Application.StartupPath + @"/root"", topdown = False):
    for name in dirs:
        list1.append(os.path.basename(os.path.join(root, name)))

print(list1[model.predict_classes(img)[0]])";

            string from = Application.StartupPath + "/IC_Test.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_testcnn);

            }
        }

        private void bunifuCustomLabel1_Click(object sender, EventArgs e)
        {

        }
    }
}
