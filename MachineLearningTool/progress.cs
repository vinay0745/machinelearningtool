﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web;
using Microsoft.Win32;
using Microsoft.VisualBasic;
using System.Drawing.Imaging;
using ImageMagick;
using System.Timers;
using PopupControl;

namespace MachineLearningTool
{
    public partial class progress : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // width of ellipse
            int nHeightEllipse // height of ellipse
        );

      

        public progress()
        {
            InitializeComponent();
            //Init.ProgressColor = Color.Silver;
            dataread.ProgressColor = Color.Silver;
            modelbuild.ProgressColor = Color.Silver;
            finishing.ProgressColor = Color.Silver;
            dataread.Value = 100;
            modelbuild.Value = 100;
            finishing.Value = 100;
            inittick.Visible = false;
            datareadtick.Visible = false;
            modelbuildtick.Visible = false;
            finishtick.Visible = false;



           // timerstop.Enabled = true;
            //timerstop.Start();

            //System.Timers.Timer timer = new System.Timers.Timer(1000);
            //timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
            //timer.Start();



            //inittick.Visible = true;
            
        }

        public System.Windows.Forms.Timer timer2 = new System.Windows.Forms.Timer();
        public System.Windows.Forms.Timer timer3 = new System.Windows.Forms.Timer();
        public System.Windows.Forms.Timer timer4 = new System.Windows.Forms.Timer();
        public System.Windows.Forms.Timer timer5 = new System.Windows.Forms.Timer();
        public System.Windows.Forms.Timer timer6 = new System.Windows.Forms.Timer();

       // public System.Windows.Forms.Timer timerstop = new System.Windows.Forms.Timer();

        public void init_timer(object sender, EventArgs e)
        {
            Console.WriteLine("Init_timer");
            dataread.animated = true;
            modelbuild.animated = true;
            finishing.animated = true;
            

            inittick.Visible = false;
            dataread.Enabled = false;
            modelbuild.Enabled = false;
            finishing.Enabled = false;
            datareadtick.Enabled = false;
            modelbuildtick.Enabled = false;
            finishtick.Enabled = false;
            bunifuProgressBar1.Enabled = false;
            bunifuProgressBar2.Enabled = false;
            bunifuProgressBar3.Enabled = false;
            timer2.Stop();
           // timer2 = null;
            timer2.Dispose();


            

            timer3.Tick += new EventHandler(dataread_timer);
            timer3.Interval = 4000;
            timer3.Start();



            
        }

        public void dataread_timer(object sender, EventArgs e)
        {
            Console.WriteLine("dataread_timer");
            inittick.Visible = true;
            label4.Visible = false;
            bunifuProgressBar1.Value = 100;
            Initiate.ForeColor = Color.Green;

            dataread.Value = 60;
            //dataread.Visible = true;
            dataread.ProgressColor = Color.SteelBlue;
            dataread.animated = true;
            if(MachineLearningTool.Form1.modelname == "IC")
            {
                if (!Directory.Exists(Application.StartupPath + @"\root"))
                {
                    Console.WriteLine("root folder nai aya");
                    //this.Close();
                }
                else
                {
                    timer3.Stop();
                   // timer3 = null;
                    timer3.Dispose();
                    //MessageBox.Show("Data reading error !!!");
                    //this.Close();
                }
            }
            else
            {
                timer3.Stop();
               // timer3 = null;
                timer3.Dispose();
                //MessageBox.Show("Data reading error !!!");
                //this.Close();
            }
            //datareadtick.Visible = true;
            //label5.Visible = false;
            //bunifuProgressBar2.Value = 100;
            //datareading.ForeColor = Color.Green;
            timer4.Tick += new EventHandler(modelbuild_timer);
            timer4.Interval = MachineLearningTool.Form1.elapsedms1 + 10000; 
            timer4.Start();

        }

        public void modelbuild_timer(object sender, EventArgs e)
        {
            Console.WriteLine("modelbuild_timer");
            // ProcessThread.Sleep(100);
            System.Threading.Thread.Sleep(100);
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();

            datareadtick.Visible = true;
            label5.Visible = false;
            bunifuProgressBar2.Value = 100;
            datareading.ForeColor = Color.Green;
           
            

            modelbuild.Value = 60;
            //dataread.Visible = true;
            modelbuild.ProgressColor = Color.SteelBlue;
            modelbuild.animated = true;



            // timer4.Stop();
            if (MachineLearningTool.Form1.modelname == "IC")
            {
                if (!File.Exists(Application.StartupPath + @"\cnn.h5"))
                {

                    Console.WriteLine("cnn file nai bana1");
                }
                else
                {
                    timer4.Stop();
                    //exitflag = true;
                   // timer4 = null;
                    timer4.Dispose();
                }
            }
           
           else if(MachineLearningTool.Form1.modelname == "SGD")
            {
                if (!File.Exists(Application.StartupPath + @"\SGD.pkl"))
                {

                    Console.WriteLine("SGD");
                }
                else
                {
                    timer4.Stop();
                   // timer4 = null;
                    timer4.Dispose();
                }
            }
            else if (MachineLearningTool.Form1.modelname == "LinearRegression")
            {
                if (!File.Exists(Application.StartupPath + @"\LinearRegression.pkl"))
                {
                    Console.WriteLine("LR");

                }
                else
                {
                    timer4.Stop();
                   // timer4 = null;
                    timer4.Dispose();
                }
            }
            else if (MachineLearningTool.Form1.modelname == "DTR")
            {
                if (!File.Exists(Application.StartupPath + @"\DTR.pkl"))
                {
                    Console.WriteLine("DTR");
                }
                else
                {
                    timer4.Stop();
                   // timer4 = null;
                    timer4.Dispose();
                }
            }

            else if (MachineLearningTool.Form1.modelname == "RFR")
            {
                if (!File.Exists(Application.StartupPath + @"\RFR.pkl"))
                {
                    Console.WriteLine("RFR");
                }
                else
                {
                    timer4.Stop();
                   // timer4 = null;
                    timer4.Dispose();
                }
            }
            else if (MachineLearningTool.Form1.modelname == "Perceptron")
            {
                if (!File.Exists(Application.StartupPath + @"\Perceptron.pkl"))
                {
                    Console.WriteLine("Perceptron");
                }
                else
                {
                    timer4.Stop();
                   // timer4 = null;
                    timer4.Dispose();
                }
            }
            else if (MachineLearningTool.Form1.modelname == "DTC")
            {
                if (!File.Exists(Application.StartupPath + @"\DTC.pkl"))
                {
                    Console.WriteLine("DTC");
                }
                else
                {
                    timer4.Stop();
                  //  timer4 = null;
                    timer4.Dispose();
                }
            }
            else if (MachineLearningTool.Form1.modelname == "RFC")
            {
                if (!File.Exists(Application.StartupPath + @"\RFC.pkl"))
                {
                    Console.WriteLine("DTR");
                }
                else
                {
                    timer4.Stop();
                   // timer4 = null;
                    timer4.Dispose();
                }
            }
            else
            {
               
                timer4.Stop();
                //timer4 = null;
                timer4.Dispose();
                //MessageBox.Show("Model building error !!!");
                
            }


           
                timer5.Tick += new EventHandler(finishing_timer);
                timer5.Interval = MachineLearningTool.Form1.elapsedms1 + 4000;
                timer5.Start();
            
             
            
           

        }

        public void finishing_timer(object sender, EventArgs e)
        {
            Console.WriteLine("finishing_timer");
            System.Threading.Thread.Sleep(100);
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();
            //Application.DoEvents();
            modelbuildtick.Visible = true;
            label1.Visible = false;
            bunifuProgressBar3.Value = 100;
            modelbuilding.ForeColor = Color.Green;


            finishing.Value = 60;
            //dataread.Visible = true;
            finishing.ProgressColor = Color.SteelBlue;
            finishing.animated = true;
            if(MachineLearningTool.Form1.modelname == "IC")
            {
                if (!File.Exists(Application.StartupPath + @"\cnn.h5"))
                {

                    Console.WriteLine("cnn file nai bana2");
                   // finishing.animated = true;
                }

                else
                {
                    timer5.Stop();
                   // timer5 = null;
                    timer5.Dispose();
                    //finishing.animated = true;
                    //MessageBox.Show("Model building error !!!");

                }
            }
            else if (MachineLearningTool.Form1.modelname == "LinearRegression")
            {
                if (!File.Exists(Application.StartupPath + @"\LinearRegression.pkl"))
                {
                    Console.WriteLine("LR");
                   // finishing.animated = true;

                }
                else
                {
                    timer5.Stop();
                   // timer5 = null;
                    timer5.Dispose();
                    //finishing.animated = true;
                }
            }
            else if (MachineLearningTool.Form1.modelname == "SGD")
            {
                if (!File.Exists(Application.StartupPath + @"\SGD.pkl"))
                {

                    Console.WriteLine("SGD");
                    //finishing.animated = true;
                }
                else
                {
                    timer5.Stop();
                   // timer5 = null;
                    timer5.Dispose();
                   // finishing.animated = true;
                }
            }
            else if (MachineLearningTool.Form1.modelname == "DTR")
            {
                if (!File.Exists(Application.StartupPath + @"\DTR.pkl"))
                {
                    Console.WriteLine("DTR");
                   // finishing.animated = true;
                }
                else
                {
                    timer5.Stop();
                   // timer5 = null;
                    timer5.Dispose();
                   // finishing.animated = true;
                }
            }

            else if (MachineLearningTool.Form1.modelname == "RFR")
            {
                if (!File.Exists(Application.StartupPath + @"\RFR.pkl"))
                {
                    Console.WriteLine("RFR");
                   // finishing.animated = true;
                }
                else
                {
                    timer5.Stop();
                   // timer5 = null;
                    timer5.Dispose();
                  //  finishing.animated = true;
                }
            }
            else if (MachineLearningTool.Form1.modelname == "Perceptron")
            {
                if (!File.Exists(Application.StartupPath + @"\Perceptron.pkl"))
                {
                    Console.WriteLine("Perceptron");
                   // finishing.animated = true;
                }
                else
                {
                    timer5.Stop();
                   // timer5 = null;
                    timer5.Dispose();
                   // finishing.animated = true;
                }
            }
            else if (MachineLearningTool.Form1.modelname == "DTC")
            {
                if (!File.Exists(Application.StartupPath + @"\DTC.pkl"))
                {
                    Console.WriteLine("DTC");
                  //  finishing.animated = true;
                }
                else
                {
                    
                    timer5.Stop();
                   // timer5 = null;
                    timer5.Dispose();
                   // finishing.animated = true;
                }
            }
            else if (MachineLearningTool.Form1.modelname == "RFC")
            {
                if (!File.Exists(Application.StartupPath + @"\RFC.pkl"))
                {
                    Console.WriteLine("DTR");
                  //  finishing.animated = true;
                }
                else
                {
                    timer5.Stop();
                   // timer5 = null;
                    timer5.Dispose();
                   // finishing.animated = true;
                }
            }
            else
            {
                timer5.Stop();
               // timer5 = null;
                timer5.Dispose();
               // finishing.animated = true;
                //MessageBox.Show("Model building error !!!");

            }
            

           if(timer5.Enabled == false)
            {
                finishtick.Visible = true;
                label2.Visible = false;
                finished.Text = "Finished";
                finished.ForeColor = Color.Green;
               
                timer6.Tick += new EventHandler(end_timer);
                timer6.Interval =2000;
                timer6.Start();
               

            }

            //timer6.Tick += new EventHandler(end_timer);
            //timer6.Interval = MachineLearningTool.Form1.elapsedms1;
            //timer6.Start();
        }
        public static bool trdstop = false;
        public void end_timer(object sender, EventArgs e)
        {
            Console.WriteLine("end_timer");
            //finishtick.Visible = true;
            //finished.Text = "Finished";
            //finished.ForeColor = Color.Green;
            //using (System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath + @"\loading.txt"))
            //{

            //    file.WriteLine("Done");

            //}
            finishing.animated = true;
            timer6.Stop();

          //  timer6 = null;
            //exitflag = true;
            timer6.Dispose();
            if (timer6.Enabled == false)
            {
                trdstop = true;
                Console.WriteLine("khatam");
                //using (System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath + @"\loading.txt"))
                //{
               // while (exitflag == false)
               // {
               //     Application.DoEvents();
               // }
                //    file.WriteLine("Done");

                //}
                //timerstop.Stop();
                // timerstop.Dispose();
                
                this.Close();
               // this.Dispose();
                //Application.Run();
               // Application.ExitThread();
                //this.Close();
                //MachineLearningTool.Form1.trd.Abort();
            }
            
            //this.Close();
        }
       // async void 
        private void progress_Load(object sender, EventArgs e)
        {
            System.IntPtr ptr = CreateRoundRectRgn(0, 0, this.Width, this.Height, 40, 40); 
            this.Region = System.Drawing.Region.FromHrgn(ptr);
          
        }
      

        private void progress_Activated(object sender, EventArgs e)
        {
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();
        }

        private void progress_VisibleChanged(object sender, EventArgs e)
        {
            timer2.Tick += new EventHandler(init_timer);
            timer2.Interval = 4000;
            timer2.Start();
           
            
        }

        private void bunifuProgressBar2_progressChanged(object sender, EventArgs e)
        {

        }
    }
}
