﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Web;
using Microsoft.Win32;
using Microsoft.VisualBasic;
using PopupControl;
using System.Drawing.Imaging;
using ImageMagick;
using System.Net;
using System.Xml;
using System.Management;
using System.Net.Http;
using System.Xml.Linq;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.Net.Cache;
//using SoftwareLocker;


namespace MachineLearningTool
{
    public partial class Form1 : Form
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect,     // x-coordinate of upper-left corner
            int nTopRect,      // y-coordinate of upper-left corner
            int nRightRect,    // x-coordinate of lower-right corner
            int nBottomRect,   // y-coordinate of lower-right corner
            int nWidthEllipse, // width of ellipse
            int nHeightEllipse // height of ellipse
        );

        [DllImport("Kernel32.dll")]
        static extern long GetTickCount64();
        public string osstarttime;

        int i;
        public string text;
        public string data;
        public static int elapsedms1 = 0;
        BackgroundWorker m_oWorker;
        public string ext;
        public bool installed;
        public string datafilename;
        public string pythonpath;
        public string result;
        public static string modelname;
        public static string[] mystring;
        public static List<string> columns;
        public Thread trd;
        public Thread cmdtrd;
        Process process = new Process();
        public static string pypath;
        public static List<string> features = new List<string>();
        public static List<string> labels = new List<string>();
        public static List<string> feat = new List<string>();
        public static Bunifu.Framework.UI.BunifuCheckbox o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, o18, o19, o20;
        public static Bunifu.Framework.UI.BunifuCheckbox i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i26, i17, i18, i19, i20;
        public List<Bunifu.Framework.UI.BunifuCheckbox> icheckboxlist = new List<Bunifu.Framework.UI.BunifuCheckbox> { i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14, i15, i26, i17, i18, i19, i20 };
        public List<Bunifu.Framework.UI.BunifuCheckbox> ocheckboxlist = new List<Bunifu.Framework.UI.BunifuCheckbox> { o1, o2, o3, o4, o5, o6, o7, o8, o9, o10, o11, o12, o13, o14, o15, o16, o17, o18, o19, o20 };
        public static Label l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15, l16, l17, l18, l19, l20;
        public List<Label> labellist = new List<Label> { l1, l2, l3, l4, l5, l6, l7, l8, l9, l10, l11, l12, l13, l14, l15, l16, l17, l18, l19, l20 };
        public string licfile = Application.StartupPath + "\\mltool001.reg";
        public int count;
        public string pathlog = Application.StartupPath + "//log.txt";
        public string hideinfo;
        public int runed;
        public string info;
        public string todaydate;
        public string log;
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        public string internettodaydate;
        // BackgroundWorker m_oWorker1;
        public Form1()
        {


            CheckForInternetConnection();
            InitializeComponent();
           
            //GetWindowsScaling();
            //float widthRatio = Screen.PrimaryScreen.Bounds.Width / 1280;
            //float heightRatio = Screen.PrimaryScreen.Bounds.Height / 800f;
            //SizeF scale = new SizeF(widthRatio, heightRatio);
            //this.Scale(scale);
            //foreach (Control control in this.Controls)
            //{
            //    control.Font = new System.Drawing.Font("Verdana", control.Font.SizeInPoints * heightRatio * widthRatio);
            //}
            //this.Width = Screen.PrimaryScreen.WorkingArea.Width / 2;
            //this.Height = Screen.PrimaryScreen.WorkingArea.Height / 2;
            //this.Top = (Screen.PrimaryScreen.WorkingArea.Top + Screen.PrimaryScreen.WorkingArea.Height) / 4;
            //this.Left = (Screen.PrimaryScreen.WorkingArea.Left + Screen.PrimaryScreen.WorkingArea.Width) / 4;

            validation();
            DateTime osStartTime = DateTime.Now - new TimeSpan(10000 * GetTickCount64());
            osstarttime = Convert.ToString(osStartTime);
            //Console.WriteLine(osstarttime);
            
            writelogfile();
            
            try
            {
                string registry_key = @"Software\Python\PythonCore\3.6";
                using (Microsoft.Win32.RegistryKey key = Registry.CurrentUser.OpenSubKey(registry_key))
                {

                    foreach (string subkey_name in key.GetSubKeyNames())
                    {

                        using (RegistryKey subkey = key.OpenSubKey(subkey_name))
                        {

                            object o = subkey.GetValue("ExecutablePath");
                            if (o != null)
                            {

                                Console.WriteLine(o);
                                //keys.Add(o.ToString());
                                pypath = o.ToString();
                            }
                        }
                    }
                }
                installed = false;
            }
            catch
            {
                installed = true;
                MessageBox.Show("Python path not found");
                
                this.Close();
            }
            //Set();

          //  Console.WriteLine(pypath.Substring(0,54) + @"Scripts\pip.exe");

            if(installed == false)
            {
                if(File.Exists(Application.StartupPath + @"\installed.reg"))
                {
                    Console.WriteLine("All libraries are installed");
                    goto SkipToEnd;
                }
                else
                {
                    
                    MessageBox.Show("Wait for libraries to be installed and click ok");
                    Set();
                    if (CheckForInternetConnection() == true)
                    {
                      
                        libraries();
                       
                        
                    }
                    //m_oWorker1.RunWorkerAsync();
                    else
                    {
                        MessageBox.Show("Make sure you have valid Internet Connection");
                        this.Close();
                    }
                   // this.Show();
                   // this.Hide();
                    
                }

                SkipToEnd:
                Console.WriteLine ("All libraries are installed 2");


            }
            else
            {
                Console.WriteLine("Libraries not installed");
            }

            

            m_oWorker = new BackgroundWorker();
            m_oWorker.DoWork += new DoWorkEventHandler(run_cmd);
            label5.Text = bunifuSlider1.Value.ToString();
            output.Text = "";
            traintime.Text = "";
            //bunifuThinButton22.Enabled = false;
            (new Bunifu.Utils.DropShaddow()).ApplyShadows(this);
            try
            {
                System.IO.File.Delete(Application.StartupPath + @"/SGD.pkl");
                System.IO.File.Delete(Application.StartupPath + @"/LinearRegression.pkl");
                System.IO.File.Delete(Application.StartupPath + @"/DTR.pkl");
                System.IO.File.Delete(Application.StartupPath + @"/RFR.pkl");
                System.IO.File.Delete(Application.StartupPath + @"/Perceptron.pkl");
                System.IO.File.Delete(Application.StartupPath + @"/DTC.pkl");
                System.IO.File.Delete(Application.StartupPath + @"/RFC.pkl");
                System.IO.File.Delete(Application.StartupPath + @"/cnn.h5");
                System.IO.File.Delete(Application.StartupPath + @"/cnn.json");
                System.IO.File.Delete(Application.StartupPath + @"/DTC_confusion.png");
                System.IO.File.Delete(Application.StartupPath + @"/Perceptron_confusion.png");
                System.IO.File.Delete(Application.StartupPath + @"/RFC_confusion.png");
                System.IO.File.Delete(Application.StartupPath + @"/IC.py");
                System.IO.File.Delete(Application.StartupPath + @"/SGD.py");
                System.IO.File.Delete(Application.StartupPath + @"/DTR.py");
                System.IO.File.Delete(Application.StartupPath + @"/LinearRegression.py");
                System.IO.File.Delete(Application.StartupPath + @"/RFR.py");
                System.IO.File.Delete(Application.StartupPath + @"/Perceptron.py");
                System.IO.File.Delete(Application.StartupPath + @"/DTC.py");
                System.IO.File.Delete(Application.StartupPath + @"/RFC.py");
                System.IO.File.Delete(Application.StartupPath + @"/Outputhistory.txt");
                string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
                foreach (string filename in pathproject)
                {
                    System.IO.File.Delete(filename);

                }

                string[] pathproject1 = Directory.GetFiles(Application.StartupPath, "*.csv");
                foreach (string filename in pathproject1)
                {
                    System.IO.File.Delete(filename);

                }
                System.IO.File.Delete(Application.StartupPath + @"/" + modelname + ".py");
                System.IO.File.Delete(Application.StartupPath + @"/Pdfs/ML_report.pdf");
                Directory.Delete(Application.StartupPath + @"/Pdfs");
               // System.IO.File.Delete(Application.StartupPath + @"/test_output.txt");
                
                
            }
            catch
            {
                Console.WriteLine("no model file found");
            }
            try
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(Application.StartupPath + @"\root");

                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
                Directory.Delete(Application.StartupPath + @"\root");
            }
            catch
            {
                Console.WriteLine("sab delete hua haha");
            }
            //create_IC();
            label8.Enabled = false;
            label9.Enabled = false;
            label10.Enabled = false;
            label11.Enabled = false;
            label12.Enabled = false;
            label13.Enabled = false;
            label14.Enabled = false;
            bunifuImageButton1.Enabled = false;
            bunifuImageButton2.Enabled = false;
            bunifuImageButton3.Enabled = false;
            bunifuImageButton4.Enabled = false;
            bunifuImageButton5.Enabled = false;
            bunifuImageButton6.Enabled = false;
            bunifuImageButton7.Enabled = false;

            bunifuSlider1.Enabled = false;
            label4.Enabled = false;
            bunifuCustomLabel1.Enabled = false;
            bunifuCustomLabel2.Enabled = false;
            label5.Enabled = false;
            label19.Enabled = false;
            IC.Enabled = false;
            label21.Enabled = false;
            bunifuImageButton8.Enabled = false;

            inputpanel1.Enabled = false;
            inputpanel1.Controls.Clear();
            panel4.Enabled = false;
            label2.Enabled = false;
            panel5.Enabled = false;
            outputpanel1.Enabled = false;
            outputpanel1.Controls.Clear();
            label3.Enabled = false;
        }
        public static class Utility
        {


            public static void fitFormToScreen(Form form, int h, int w)
            {

                //scale the form to the current screen resolution
                form.Height = (int)((float)form.Height * ((float)Screen.PrimaryScreen.Bounds.Size.Height / (float)h));
                form.Width = (int)((float)form.Width * ((float)Screen.PrimaryScreen.Bounds.Size.Width / (float)w));

                //here font is scaled like width
                //form.Font = new System.Drawing.Font(form.Font.FontFamily, form.Font.Size * ((float)Screen.PrimaryScreen.Bounds.Size.Width / (float)w));

                foreach (Control item in form.Controls)
                {
                    fitControlsToScreen(item, h, w);
                }

            }

            static void fitControlsToScreen(Control cntrl, int h, int w)
            {
                if (Screen.PrimaryScreen.Bounds.Size.Height != h)
                {

                    cntrl.Height = (int)((float)cntrl.Height * ((float)Screen.PrimaryScreen.Bounds.Size.Height / (float)h));
                    cntrl.Top = (int)((float)cntrl.Top * ((float)Screen.PrimaryScreen.Bounds.Size.Height / (float)h));

                }
                if (Screen.PrimaryScreen.Bounds.Size.Width != w)
                {

                    cntrl.Width = (int)((float)cntrl.Width * ((float)Screen.PrimaryScreen.Bounds.Size.Width / (float)w));
                    cntrl.Left = (int)((float)cntrl.Left * ((float)Screen.PrimaryScreen.Bounds.Size.Width / (float)w));

                    //cntrl.Font = new System.Drawing.Font(cntrl.Font.FontFamily, cntrl.Font.Size * ((float)Screen.PrimaryScreen.Bounds.Size.Width / (float)w));

                }

                foreach (Control item in cntrl.Controls)
                {
                    fitControlsToScreen(item, h, w);
                }
            }
        }


        //inside form load event
        //send the width and height of the screen you designed the form for
       
        
        public void readlogfile()
        {
            hideinfo = FileReadWrite.ReadFile(pathlog);
            return;
        }

        public void writelogfile()
        {
            if (!File.Exists(pathlog))
            {
                string checklisttxt = Convert.ToString(count);
                Console.WriteLine(checklisttxt);
                FileReadWrite.WriteFile(pathlog, checklisttxt);
            }

            else
            {

                string run = FileReadWrite.ReadFile(pathlog);
                readlogfile();
                runed = Convert.ToInt32(run) + 1;
                todaydate = Convert.ToString(DateTime.Now.Date);

                if (osstarttime != string.Empty)
                {
                    info = runed + ";" + osstarttime;
                    FileReadWrite.WriteFile(pathlog, Convert.ToString(runed));
                    Console.WriteLine(info);
                }
                else
                {
                    info = runed + ";" + todaydate;
                    FileReadWrite.WriteFile(pathlog, Convert.ToString(runed));
                    Console.WriteLine(info);
                }


            }

        }

        
        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                using (client.OpenRead("http://google.com/generate_204"))
                    return true;
            }
            catch
            {
                return false;
            }
        }


        public void datenowtime()
        {
            try
            {
                var client = new TcpClient("time.nist.gov", 13);
                using (var streamReader = new StreamReader(client.GetStream()))
                {
                    var response = streamReader.ReadToEnd();
                    var utcDateTimeString = response.Substring(7, 17);
                    //Console.WriteLine(response);
                    //Console.WriteLine(utcDateTimeString);
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(Convert.ToDateTime(utcDateTimeString), INDIAN_ZONE);
                    internettodaydate = Convert.ToString(indianTime);
                    // Console.WriteLine(internettodaydate);
                    //Console.WriteLine(indianTime);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("No internet");
            }

        }


        public void validation()
        {

            if (File.Exists(licfile))
            {
                try
                {
                    DateTime creation = File.GetCreationTime(licfile);
                    DateTime enddate = creation.AddDays(1);
                    string one = internettodaydate.Substring(0, 2);
                    string two = internettodaydate.Substring(2, 6);
                    string three = internettodaydate.Substring(8, 10);
                    string[] four = three.Split(' ');
                    foreach (string lined in four)
                    {
                        log = string.Concat(log, lined);
                    }
                    string endates = Convert.ToString(enddate).Substring(0, 10);
                    string todaydates = Convert.ToString(todaydate).Substring(0, 10);
                    if (endates == log.Substring(0, 2) + two + one)
                    {
                        System.IO.File.Delete(licfile);
                    }
                    else if (endates == todaydates)
                    {
                        System.IO.File.Delete(licfile);
                    }
                    else if (Convert.ToDateTime(endates) < Convert.ToDateTime(log.Substring(0, 2) + two + one))
                    {
                        System.IO.File.Delete(licfile);
                    }
                    else if (enddate < DateTime.Now)
                    {
                        System.IO.File.Delete(licfile);
                    }
                    else if (enddate < Convert.ToDateTime(osstarttime))
                    {
                        System.IO.File.Delete(licfile);
                    }
                }
                catch (Exception)
                {
                    Console.WriteLine("internettodaydate was empty");
                }
            }
            else
            {
               // goto 
                Console.WriteLine("License hai");
                //this.Close();
            }
            //       Trialmaker t = new Trialmaker("enblarpre",
            //Application.StartupPath + "\\enb0112p.reg",
            //Environment.GetFolderPath(Environment.SpecialFolder.System) +
            //  "\\enb0112p.dbf",
            //"E-mail: contact@eduvance.in",
            //365, 100, "112");

            //      byte[] MyOwnKey = { 97, 250,  1,  5,  84, 21,   7, 63,
            //                   4,  54, 87, 56, 123, 10,   3, 62,
            //                   7,   9, 20, 36,  37, 21, 101, 57};
            //      t.TripleDESKey = MyOwnKey;
            //      // if you don't call this part the program will
            //      //use default key to encryption

            //      Trialmaker.RunTypes RT = t.ShowDialog();
            //      bool is_trial;
            //      if (RT != Trialmaker.RunTypes.Expired)
            //      {
            //          if (RT == Trialmaker.RunTypes.Full)
            //              is_trial = false;
            //          else
            //              is_trial = true;

            //          Application.Exit();
            //          Application.Restart();
            //      }
        }

        public void libraries() //object sender, DoWorkEventArgs e
        {
            create_libinstall();
            this.Hide();
           // this.Invoke((MethodInvoker)delegate () { 
            ProcessStartInfo start = new ProcessStartInfo();
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.CreateNoWindow = true;
            start.UseShellExecute = false;
            start.Verb = "runas";
            start.FileName = "cmd.exe";
            start.Arguments = string.Format(@"/C "+ pypath + " " + Application.StartupPath + @"/Libinstall.py");
            start.RedirectStandardOutput = true;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string pdf = reader.ReadToEnd();
                    Console.WriteLine(pdf);
                    // MessageBox.Show("Pdf file generated and will be save on your Desktop Folder");
                   // using (System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath + @"\installed.reg"))
                   // {
                  // if(process.HasExited && process.ExitCode == 0)
                   // {
                        
                  //  }
                    

                    //}
                }
               

            }
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath + @"\installed.reg"))
            {

                file.WriteLine("Andi pandi sandi jo ye khola to tu randi");
                System.IO.File.Delete(Application.StartupPath + @"/Libinstall.py");

            }
            //});
            this.Show();
        }

        
        private void bunifuCheckbox2_OnChange(object sender, EventArgs e)
        {

        }

        private void Algorithmpanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label14_Click(object sender, EventArgs e)
        {

        }

        private void bunifuCheckbox6_OnChange(object sender, EventArgs e)
        {

        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void bunifuCheckbox5_OnChange(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
           // Utility.fitFormToScreen(this, this.Height, this.Width);
            //this.CenterToScreen();
            // this.WindowState = FormWindowState.Maximized;
            // this.Location = new Point(0, 0);
            // this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            //System.IntPtr ptr = CreateRoundRectRgn(0, 0, this.Width, this.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
            //this.Region = System.Drawing.Region.FromHrgn(ptr);
        }
        
        private void panel4_Paint(object sender, PaintEventArgs e)
        {
            System.IntPtr ptr = CreateRoundRectRgn(0, 0, panel4.Width, panel4.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
            panel4.Region = System.Drawing.Region.FromHrgn(ptr);
        
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                System.IntPtr ptr = CreateRoundRectRgn(0, 0, panel1.Width, panel1.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
                panel1.Region = System.Drawing.Region.FromHrgn(ptr);
            }
            catch
            {
                Console.WriteLine("Parameter is not valid");
            }
        }

        private void panel5_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                System.IntPtr ptr = CreateRoundRectRgn(0, 0, panel5.Width, panel5.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
                panel5.Region = System.Drawing.Region.FromHrgn(ptr);
            }
            catch
            {

            }
        }

        private void panel4_Resize(object sender, EventArgs e)
        {
            
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
       
      

        private void bunifuSlider1_ValueChanged(object sender, EventArgs e)
        {
            label5.Text = bunifuSlider1.Value.ToString();

            label19.Text = (100 - bunifuSlider1.Value).ToString();

           

        }


        public void  readcsvfile()
        {
           
                using (StreamReader SR = new StreamReader(datafilename))
                {
                    string readingline = SR.ReadLine();
                    mystring = readingline.Split(',');
                }

                for (int i = 0; i < mystring.Length; i++)
                {
                    columns = mystring.ToList<string>();
                }
          
            

        }
        public void Set()
        {
            const string name = "PATH";
            string pathvar = System.Environment.GetEnvironmentVariable(name);
            var value = pathvar + @";" + pythonpath + ";";
            var target = EnvironmentVariableTarget.Machine;
            System.Environment.SetEnvironmentVariable(name, value, target);
            Console.WriteLine(target);
        }

       
        private void Browsebutton_Click(object sender, EventArgs e)
        {

            run.ActiveFillColor = Color.DodgerBlue;
            run.ActiveLineColor = Color.DodgerBlue;
            run.IdleForecolor = Color.DodgerBlue;
            run.IdleLineColor = Color.DodgerBlue;
            string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
            foreach (string filename in pathproject)
            {
                System.IO.File.Delete(filename);

            }

            string[] pathproject1 = Directory.GetFiles(Application.StartupPath, "*.csv");
            foreach (string filename in pathproject1)
            {
                System.IO.File.Delete(filename);

            }
            features.Clear();
            labels.Clear();
            feat.Clear();
            inputpanel1.Controls.Clear();
            outputpanel1.Controls.Clear();
            
            System.Windows.Forms.OpenFileDialog openfiledialog = new System.Windows.Forms.OpenFileDialog();
            openfiledialog.Filter = "CSV files (*.csv)|*.csv|Zip files (*.zip)|*.zip";
            
            if (openfiledialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                
                datafilename = openfiledialog.FileName;
                System.IO.File.Copy(datafilename, Application.StartupPath+@"\"+openfiledialog.SafeFileName, true);
                ext = Path.GetExtension(openfiledialog.FileName);
                Console.WriteLine(datafilename);
                if(ext == ".zip")
                {
                    inputpanel1.Enabled = false;
                    inputpanel1.Controls.Clear();
                    panel4.Enabled = false;
                    label2.Enabled = false;
                    panel5.Enabled = false;
                    outputpanel1.Enabled = false;
                    outputpanel1.Controls.Clear();
                    label3.Enabled = false;

                    label8.Enabled = false;
                    label9.Enabled = false;
                    label10.Enabled = false;
                    label11.Enabled = false;
                    label12.Enabled = false;
                    label13.Enabled = false;
                    label14.Enabled = false;
                    bunifuImageButton1.Enabled = false;
                    bunifuImageButton2.Enabled = false;
                    bunifuImageButton3.Enabled = false;
                    bunifuImageButton4.Enabled = false;
                    bunifuImageButton5.Enabled = false;
                    bunifuImageButton6.Enabled = false;
                    bunifuImageButton7.Enabled = false;

                    bunifuSlider1.Enabled = true;
                    label4.Enabled = true;
                    bunifuCustomLabel1.Enabled = true;
                    bunifuCustomLabel2.Enabled = true;
                    label5.Enabled = true;
                    label19.Enabled = true;

                    IC.Enabled = true;
                    label21.Enabled = true;
                    bunifuImageButton8.Enabled = true;

                   
                    LinearRegression.Enabled = false;
                    LinearRegression.Checked = false;
                    
                    SGD.Enabled = false;
                    SGD.Checked = false;
                    
                    DTR.Enabled = false;
                    DTR.Checked = false;

                    RFR.Enabled = false;
                    RFR.Checked = false;

                    Perceptron.Enabled = false;
                    Perceptron.Checked = false;

                    DTC.Enabled = false;
                    DTC.Checked = false;
                    
                    RFC.Enabled = false;
                    RFC.Checked = false;
                }
                else if(ext == ".csv")
                {
                    inputpanel1.Enabled = true;
                    panel4.Enabled = true;
                    label2.Enabled = true;
                    panel5.Enabled = true;
                    outputpanel1.Enabled = true;
                    label3.Enabled = true;

                    label8.Enabled = true;
                    label9.Enabled = true;
                    label10.Enabled = true;
                    label11.Enabled = true;
                    label12.Enabled = true;
                    label13.Enabled = true;
                    label14.Enabled = true;
                    bunifuImageButton1.Enabled = true;
                    bunifuImageButton2.Enabled = true;
                    bunifuImageButton3.Enabled = true;
                    bunifuImageButton4.Enabled = true;
                    bunifuImageButton5.Enabled = true;
                    bunifuImageButton6.Enabled = true;
                    bunifuImageButton7.Enabled = true;

                    bunifuSlider1.Enabled = true;
                    label4.Enabled = true;
                    bunifuCustomLabel1.Enabled = true;
                    bunifuCustomLabel2.Enabled = true;
                    label5.Enabled = true;
                    label19.Enabled = true;

                    IC.Enabled = false;
                    IC.Checked = false;
                    label21.Enabled = false;
                    bunifuImageButton8.Enabled = false;

                    LinearRegression.Enabled = true;
                    SGD.Enabled = true;
                    DTR.Enabled = true;
                    RFR.Enabled = true;
                    Perceptron.Enabled = true;
                    DTC.Enabled = true;
                    RFC.Enabled = true;

                    try
                    {
                        readcsvfile();
                        createcheckboxes();
                        createcheckboxes1();
                    }
                    catch
                    {
                        Console.WriteLine("no data file selected");
                    }
                }
            }
            openfiledialog.Dispose();
            
        }

        
        public void check()
        {
            foreach (Control c in inputpanel1.Controls)
            {
                if ((c is Bunifu.Framework.UI.BunifuCheckbox) && ((Bunifu.Framework.UI.BunifuCheckbox)c).Checked)
                {
                    try
                    {
                        features.Add(columns[Convert.ToInt32(c.Name)]);
                        feat.Add(columns[Convert.ToInt32(c.Name)]);
                    }
                    catch
                    {
                        Console.WriteLine("null");

                    }
                }
            }



            foreach (Control c in outputpanel1.Controls)
            {
                if ((c is Bunifu.Framework.UI.BunifuCheckbox) && ((Bunifu.Framework.UI.BunifuCheckbox)c).Checked)
                {
                    try
                    {
                        labels.Add(columns[Convert.ToInt32(c.Name)]);
                    }
                    catch
                    {
                        Console.WriteLine("null");

                    }
                }
            }

            return;
        }

        public static string getBetween(string strSource, string strStart)
        {
            int Start;
            if (strSource.Contains(strStart))
            {
                Start = strSource.IndexOf(strStart, 0);
                return strSource.Substring(Start);
                
            }
            else
            {
                return "";
            }
        }

 
        
        private void run_Click(object sender, EventArgs e)
        {
            output.Text = "";
            confusionmatbox.Image = null;
            traintime.Text = "";

            try
            {
                System.IO.File.Delete(Application.StartupPath + @"/Pdfs/ML_report.pdf");
                Directory.Delete(Application.StartupPath + @"/Pdfs");
            }
            catch
            {

            }

            //if (bunifuSlider1.Value > 40)
            //{
            if(run.ActiveLineColor == Color.Green)
            {
                MessageBox.Show("Select Algorithm again to Run");
                return;
            }
            //run.ActiveFillColor = Color.DodgerBlue;
            //run.ActiveLineColor = Color.DodgerBlue;
            //run.IdleForecolor = Color.DodgerBlue;
            //run.IdleLineColor = Color.DodgerBlue;
            else
            {
              
                

                //var pro = new progress();

                //trd.IsBackground = true;

                //this.Hide();
                this.Visible = false;
                //Application.Run(new progress());

                trd = new Thread(() =>
                {
                    loadingscreen();

                });

                cmdtrd = new Thread(() => 
                {

                    m_oWorker.RunWorkerAsync();
                });
                

               Parallel.Invoke(() =>
               {
                   trd.Start();
                   //loadingscreen();
               },
               () => {
                   cmdtrd.Start();
               });


                //backgroundWorker1.DoWork += new DoWorkEventArgs(run_cmd);
                //trd1 = new Thread(() => 
                //{
                //    run_cmd();

                //});
                //trd.IsBackground = true;
                //loadingrun();
                // trd.Start();
                // starttask();
                //trd1.Start();
                //trd.Join();

                //trd1.Join();
                //while(trd == null)
                //{
                // trd.Abort();
                //this.Show();
                //Console.WriteLine(trd.ThreadState);
                // }
                // else
                // {

                //trd.Join();
                //trd1.Join();


                //System.IO.File.Delete(Application.StartupPath + @"/loading.txt");

                //bunifuThinButton22.Enabled = true;
                //System.IO.File.Delete(Application.StartupPath + @"/Outputhistory.txt");
                //LinearRegression.Checked = false;
                //SGD.Checked = false;
                //DTR.Checked = false;
                //RFR.Checked = false;
                //RFC.Checked = false;
                //DTC.Checked = false;
                //Perceptron.Checked = false;
                //IC.Checked = false;
                //string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
                //foreach (string filename in pathproject)
                //{
                //    System.IO.File.Delete(filename);

                //}
                //string[] pathproject1 = Directory.GetFiles(Application.StartupPath, "*.csv");
                //foreach (string filename in pathproject1)
                //{
                //    System.IO.File.Delete(filename);

                //}

            }
            //trd.Abort();
            //System.IO.File.Delete(Application.StartupPath + @"/loading.txt");


        }
        
        public void loadingscreen()
        {
           var ac = (progress)Application.OpenForms["progress"];

            ac = new progress();
            //Action action = new Action(run_cmd);
            //this.BeginInvoke(action);
            //run_cmd();
            //Application.DoEvents();
            //trd1.IsBackground = true;

            ac.ShowDialog();
            this.Invoke((MethodInvoker)delegate () {
                    
                   // Application.Run(new progress());
                    //ac = null;
                    var fileStream = new FileStream(Application.StartupPath + @"\output.txt", FileMode.Open, FileAccess.Read);
                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        text = streamReader.ReadToEnd();
                        Console.WriteLine("************");
                        if (modelname == "IC")
                        {
                            confusionmatbox.Image = null;
                            // Console.WriteLine(text.Length);
                            //confusionmatbox.ImageLocation = "";
                            consolepanel.AutoScroll = true;
                            data = getBetween(text, "Epoch:");
                            //string strStart = "model_accuracy";
                            //int Start = data.IndexOf(strStart, 0) + strStart.Length;
                            ////strStart.Remove();
                            //Console.WriteLine(Start);
                            Console.WriteLine(data);
                            output.Text = data;
                            traintime.Text = "Total Training time: " + Convert.ToInt32(elapsedms1) + " ms";
                            output.Font = new System.Drawing.Font("Segoe UI", 13.0F, FontStyle.Regular);
                            traintime.Font = new System.Drawing.Font("Segoe UI", 13.0F, FontStyle.Regular);
                            output.ForeColor = Color.Black;
                            traintime.ForeColor = Color.Black;
                            run.ActiveFillColor = Color.Green;
                            run.ActiveLineColor = Color.Green;
                            run.IdleForecolor = Color.Green;
                            run.IdleLineColor = Color.Green;
                        }
                        else
                        {

                            output.Text = text;
                            traintime.Text = "Total Training time: " + Convert.ToInt32(elapsedms1) + " ms";
                            output.Font = new System.Drawing.Font("Segoe UI", 13.0F, FontStyle.Regular);
                            traintime.Font = new System.Drawing.Font("Segoe UI", 13.0F, FontStyle.Regular);
                            output.ForeColor = Color.Black;
                            traintime.ForeColor = Color.Black;
                            run.ActiveFillColor = Color.Green;
                            run.ActiveLineColor = Color.Green;
                            run.IdleForecolor = Color.Green;
                            run.IdleLineColor = Color.Green;
                        }
                    }


                    this.Visible = true; ;
                    //trd.Abort();
                    //trd1.Abort();
                    ac.Close();
                    using (StreamWriter sw = File.AppendText(Application.StartupPath + @"\Outputhistory.txt"))
                    {

                        sw.WriteLine("Run " + i);

                        //sw.WriteLine();
                        if (modelname == "LinearRegression")
                        {
                            sw.WriteLine("Inputs: " + String.Join(",", feat));
                            sw.WriteLine("Outputs: " + String.Join(",", labels));
                            sw.WriteLine("Model: Linear Regression OLS");
                            sw.WriteLine(traintime.Text);
                            sw.WriteLine(result);
                            sw.WriteLine();
                        }
                        else if (modelname == "SGD")
                        {
                            sw.WriteLine("Inputs: " + String.Join(",", feat));
                            sw.WriteLine("Outputs: " + String.Join(",", labels));
                            sw.WriteLine("Model: Linear Regression SGD");
                            sw.WriteLine(traintime.Text);
                            sw.WriteLine(result);
                            sw.WriteLine();
                        }
                        else if (modelname == "DTR")
                        {
                            sw.WriteLine("Inputs: " + String.Join(",", feat));
                            sw.WriteLine("Outputs: " + String.Join(",", labels));
                            sw.WriteLine("Model: Decision Tree Regressor");
                            sw.WriteLine(traintime.Text);
                            sw.WriteLine(result);
                            sw.WriteLine();
                        }
                        else if (modelname == "RFR")
                        {
                            sw.WriteLine("Inputs: " + String.Join(",", feat));
                            sw.WriteLine("Outputs: " + String.Join(",", labels));
                            sw.WriteLine("Model: Random Forest Regressor");
                            sw.WriteLine(traintime.Text);
                            sw.WriteLine(result);
                            sw.WriteLine();
                        }
                        else if (modelname == "Perceptron")
                        {
                            sw.WriteLine("Inputs: " + String.Join(",", feat));
                            sw.WriteLine("Outputs: " + String.Join(",", labels));
                            sw.WriteLine("Model: Perceptron");
                            sw.WriteLine(traintime.Text);
                            sw.WriteLine(result);
                            sw.WriteLine();
                        }
                        else if (modelname == "DTC")
                        {
                            sw.WriteLine("Inputs: " + String.Join(",", feat));
                            sw.WriteLine("Outputs: " + String.Join(",", labels));
                            sw.WriteLine("Model: Decision Tree Classifer");
                            sw.WriteLine(traintime.Text);
                            sw.WriteLine(result);
                            sw.WriteLine();
                        }
                        else if (modelname == "RFC")
                        {
                            sw.WriteLine("Inputs: " + String.Join(",", feat));
                            sw.WriteLine("Outputs: " + String.Join(",", labels));
                            sw.WriteLine("Model: Random Forest Classifer");
                            sw.WriteLine(traintime.Text);
                            sw.WriteLine(result);
                            sw.WriteLine();
                        }
                        else if (modelname == "IC")
                        {
                            sw.WriteLine("Model: Image Classifer");
                            sw.WriteLine(traintime.Text);
                            sw.WriteLine(data);
                            sw.WriteLine();
                        }
                        //sw.WriteLine(traintime.Text);
                        //sw.WriteLine(result);
                        //sw.WriteLine();
                        //sw.WriteLine("Text");
                        i = +1;
                    }


                    feat = feat.Distinct().ToList();
                    
                    features.Clear();
                    labels.Clear();
                    System.IO.File.Delete(Application.StartupPath + @"/" + modelname + ".py");

                });
            ac.Close();
        }

        //public async void loadingrun()
        //{
        //    //await Task.Run(() => loadingscreen());
        //}
        //public async void starttask()
        //{
        //    //await Task.Run(() => run_cmd());
        //}
       
    
        public void run_cmd(object sender, DoWorkEventArgs e)
        {
            //this.Invoke((MethodInvoker)delegate () 
            //{
                check();
            
            foreach (Control c in Algorithmpanel.Controls)
                {
                    if ((c is Bunifu.Framework.UI.BunifuCheckbox) && ((Bunifu.Framework.UI.BunifuCheckbox)c).Checked)
                    {
                        modelname = c.Name;

                    }
                }


                if (modelname == "LinearRegression")
                {
                    create_OLS();
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.h5");
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.json");
                    System.IO.File.Delete(Application.StartupPath + @"/DTC_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/Perceptron_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/RFC_confusion.png");

                    string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
                    confusionmatbox.Image = null;
                    foreach (string filename in pathproject)
                    {
                        System.IO.File.Delete(filename);

                    }
                }
                else if (modelname == "SGD")
                {
                    create_SGD();
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.h5");
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.json");
                    System.IO.File.Delete(Application.StartupPath + @"/DTC_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/Perceptron_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/RFC_confusion.png");
                    string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
                    confusionmatbox.Image = null;
                    foreach (string filename in pathproject)
                    {
                        System.IO.File.Delete(filename);

                    }
                }
                else if (modelname == "DTR")
                {
                    create_DTR();
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.h5");
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.json");
                    System.IO.File.Delete(Application.StartupPath + @"/DTC_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/Perceptron_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/RFC_confusion.png");
                    string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
                    confusionmatbox.Image = null;
                    foreach (string filename in pathproject)
                    {
                        System.IO.File.Delete(filename);

                    }
                }
                else if (modelname == "RFR")
                {
                    create_RFR();
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.h5");
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.json");
                    System.IO.File.Delete(Application.StartupPath + @"/DTC_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/Perceptron_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/RFC_confusion.png");
                    string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
                    confusionmatbox.Image = null;
                    foreach (string filename in pathproject)
                    {
                        System.IO.File.Delete(filename);

                    }
                }
                else if (modelname == "Perceptron")
                {
                    create_Perceptron();
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.h5");
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.json");
                    System.IO.File.Delete(Application.StartupPath + @"/DTC_confusion.png");
                    //System.IO.File.Delete(Application.StartupPath + @"/Perecptron_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/RFC_confusion.png");
                    string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
                    confusionmatbox.Image = null;
                    foreach (string filename in pathproject)
                    {
                        System.IO.File.Delete(filename);

                    }
                }
                else if (modelname == "DTC")
                {
                    create_DTC();
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.h5");
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.json");
                    // System.IO.File.Delete(Application.StartupPath + @"/DTC_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/Perceptron_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/RFC_confusion.png");
                    string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
                    confusionmatbox.Image = null;
                    foreach (string filename in pathproject)
                    {
                        System.IO.File.Delete(filename);

                    }
                }
                else if (modelname == "RFC")
                {
                    create_RFC();
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.h5");
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.json");
                    string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.zip");
                    System.IO.File.Delete(Application.StartupPath + @"/DTC_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/Perceptron_confusion.png");
                    //System.IO.File.Delete(Application.StartupPath + @"/RFC_confusion.png");
                    confusionmatbox.Image = null;
                    foreach (string filename in pathproject)
                    {
                        System.IO.File.Delete(filename);

                    }
                }
                else if (modelname == "IC")
                {
                   
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.h5");
                    System.IO.File.Delete(Application.StartupPath + @"/cnn.json");
                    System.IO.File.Delete(Application.StartupPath + @"/SGD.pkl");
                    System.IO.File.Delete(Application.StartupPath + @"/LinearRegression.pkl");
                    System.IO.File.Delete(Application.StartupPath + @"/DTR.pkl");
                    System.IO.File.Delete(Application.StartupPath + @"/RFR.pkl");
                    System.IO.File.Delete(Application.StartupPath + @"/Perceptron.pkl");
                    System.IO.File.Delete(Application.StartupPath + @"/DTC.pkl");
                    System.IO.File.Delete(Application.StartupPath + @"/RFC.pkl");
                    System.IO.File.Delete(Application.StartupPath + @"/DTC_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/Perceptron_confusion.png");
                    System.IO.File.Delete(Application.StartupPath + @"/RFC_confusion.png");
                    confusionmatbox.Image = null;
                    create_IC();

                    string[] pathproject = Directory.GetFiles(Application.StartupPath, "*.csv");
                    foreach (string filename in pathproject)
                    {
                        System.IO.File.Delete(filename);

                    }
                try
                {
                    System.IO.DirectoryInfo di = new DirectoryInfo(Application.StartupPath + @"\root");

                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in di.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                    Directory.Delete(Application.StartupPath + @"\root");
                }
                catch
                {
                    Console.WriteLine("sab delete hua haha");
                }
                //try
                //{
                //    System.IO.DirectoryInfo di = new DirectoryInfo(Application.StartupPath + @"\root");

                //    foreach (FileInfo file in di.GetFiles())
                //    {
                //        file.Delete();
                //    }
                //    foreach (DirectoryInfo dir in di.GetDirectories())
                //    {
                //        dir.Delete(true);
                //    }
                //    Directory.Delete(Application.StartupPath + @"\root");
                //}
                //catch
                //{
                //    Console.WriteLine("sab delete hua haha");
                //}
            }
                //Thread.Yield();
                float split = Convert.ToInt32(label5.Text);

                //GC.Collect();
                //GC.WaitForPendingFinalizers();
                //GC.Collect();

                ProcessStartInfo start = new ProcessStartInfo();
                start.WindowStyle = ProcessWindowStyle.Hidden;
                start.CreateNoWindow = true;
                start.UseShellExecute = false;
                start.FileName = "cmd.exe";
                //Console.WriteLine(keys[0]);
                if (modelname == "IC")
                {
                    start.Arguments = string.Format(@"/C " + pypath + " " + Application.StartupPath + @"/" + modelname + ".py ");
                }
                else
                {
                    start.Arguments = string.Format(@"/C "+ pypath + " " + Application.StartupPath + @"/" + modelname + ".py " + "-d " + "r\"" + datafilename + "\"" + " -i " + String.Join(",", features) + " -o " + String.Join(",", labels) + " -s " + split / 100);
                }

                Console.WriteLine(start.Arguments);
                start.RedirectStandardOutput = true;
                var watch1 = System.Diagnostics.Stopwatch.StartNew();
                process.Exited += new EventHandler(cmd_Exited);
                using (process = Process.Start(start))
                {
                    using (StreamReader reader = process.StandardOutput)
                    {
                        result = reader.ReadToEnd();
                        Console.Write(result);
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(Application.StartupPath + @"\output.txt"))
                        {

                            file.WriteLine(result);


                        }



                    }
                    // string[] lines = System.IO.File.ReadAllLines(Application.StartupPath + @"\output.txt");


                    var fileStream = new FileStream(Application.StartupPath + @"\output.txt", FileMode.Open, FileAccess.Read);
                    using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
                    {
                        text = streamReader.ReadToEnd();
                        Console.WriteLine("************");

                        //text.Substring(5100)
                        if (modelname == "IC")
                        {

                            watch1.Stop();
                            elapsedms1 = Convert.ToInt32(watch1.ElapsedMilliseconds);

                        }
                        else
                        {
                            watch1.Stop();
                            elapsedms1 = Convert.ToInt32(watch1.ElapsedMilliseconds);



                            if (modelname == "DTC")
                            {

                                confusionmatbox.ImageLocation = Application.StartupPath + @"\DTC_confusion.png";
                            }
                            else if (modelname == "RFC")
                            {
                                confusionmatbox.ImageLocation = Application.StartupPath + @"\RFC_confusion.png";
                            }
                            else if (modelname == "Perceptron")
                            {
                                confusionmatbox.ImageLocation = Application.StartupPath + @"\Perceptron_confusion.png";
                            }
                        }


                        //process.WaitForExit();
                        if (process.HasExited && process.ExitCode == 0)
                        {
                            Console.WriteLine("process khatam");
                        }


                    }

                }

            
            
        }

        void cmd_Exited(object sender, EventArgs e)
        {
            //MessageBox.Show(output.ToString());
            process.Dispose();
        }
        private void bunifuCheckbox5_MouseClick(object sender, MouseEventArgs e)
        {
            if (RFC.Checked)
            {
                LinearRegression.Checked = false;
                SGD.Checked = false;
                RFR.Checked = false;
                Perceptron.Checked = false;
                DTC.Checked = false;
                DTR.Checked = false;
                IC.Checked = false;
                run.ActiveFillColor = Color.DodgerBlue;
                run.ActiveLineColor = Color.DodgerBlue;
                run.IdleForecolor = Color.DodgerBlue;
                run.IdleLineColor = Color.DodgerBlue;
            }
        }

        private void bunifuCheckbox6_MouseClick(object sender, MouseEventArgs e)
        {
            if (DTC.Checked)
            {
                LinearRegression.Checked = false;
                SGD.Checked = false;
                RFR.Checked = false;
                RFC.Checked = false;
                Perceptron.Checked = false;
                DTR.Checked = false;
                IC.Checked = false;
                run.ActiveFillColor = Color.DodgerBlue;
                run.ActiveLineColor = Color.DodgerBlue;
                run.IdleForecolor = Color.DodgerBlue;
                run.IdleLineColor = Color.DodgerBlue;
            }
        }

        private void bunifuCheckbox7_MouseClick(object sender, MouseEventArgs e)
        {
            if (Perceptron.Checked)
            {
                LinearRegression.Checked = false;
                SGD.Checked = false;
                RFR.Checked = false;
                RFC.Checked = false;
                DTC.Checked = false;
                DTR.Checked = false;
                IC.Checked = false;
                run.ActiveFillColor = Color.DodgerBlue;
                run.ActiveLineColor = Color.DodgerBlue;
                run.IdleForecolor = Color.DodgerBlue;
                run.IdleLineColor = Color.DodgerBlue;
            }
        }

        private void bunifuCheckbox4_MouseClick(object sender, MouseEventArgs e)
        {
            if (RFR.Checked)
            {
                LinearRegression.Checked = false;
                SGD.Checked = false;
                DTR.Checked = false;
                RFC.Checked = false;
                DTC.Checked = false;
                Perceptron.Checked = false;
                IC.Checked = false;
                run.ActiveFillColor = Color.DodgerBlue;
                run.ActiveLineColor = Color.DodgerBlue;
                run.IdleForecolor = Color.DodgerBlue;
                run.IdleLineColor = Color.DodgerBlue;
            }
        }

        private void bunifuCheckbox3_MouseClick(object sender, MouseEventArgs e)
        {
            if (DTR.Checked)
            {
                LinearRegression.Checked = false;
                SGD.Checked = false;
                RFR.Checked = false;
                RFC.Checked = false;
                DTC.Checked = false;
                Perceptron.Checked = false;
                IC.Checked = false;
                run.ActiveFillColor = Color.DodgerBlue;
                run.ActiveLineColor = Color.DodgerBlue;
                run.IdleForecolor = Color.DodgerBlue;
                run.IdleLineColor = Color.DodgerBlue;
            }
        }

        private void bunifuCheckbox2_MouseClick(object sender, MouseEventArgs e)
        {
            if (SGD.Checked)
            {
                LinearRegression.Checked = false;
                DTR.Checked = false;
                RFR.Checked = false;
                RFC.Checked = false;
                DTC.Checked = false;
                Perceptron.Checked = false;
                IC.Checked = false;
                run.ActiveFillColor = Color.DodgerBlue;
                run.ActiveLineColor = Color.DodgerBlue;
                run.IdleForecolor = Color.DodgerBlue;
                run.IdleLineColor = Color.DodgerBlue;
            }
        }

        private void bunifuCheckbox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (LinearRegression.Checked)
            {
                SGD.Checked = false;
                DTR.Checked = false;
                RFR.Checked = false;
                RFC.Checked = false;
                DTC.Checked = false;
                Perceptron.Checked = false;
                IC.Checked = false;
                run.ActiveFillColor = Color.DodgerBlue;
                run.ActiveLineColor = Color.DodgerBlue;
                run.IdleForecolor = Color.DodgerBlue;
                run.IdleLineColor = Color.DodgerBlue;
            }
        }

        

        private void bunifuThinButton22_Click(object sender, EventArgs e)
        {

            string[] fileEntries = Directory.GetFiles(Application.StartupPath + "/", "*.pkl");
            for (int i = 0; i < fileEntries.Length; i++)
            {
                if(Path.GetFileName(fileEntries[i]) == modelname+".pkl")
                {
                    Console.WriteLine(Path.GetFileName(fileEntries[i]));
                    
                }
                else
                {
                    Console.WriteLine(fileEntries[i]);
                    System.IO.File.Delete(fileEntries[i]);
                    
                }
            }
            try
            {
                if (modelname == "IC")
                {
                    var cnntest = new CnnTest();
                    this.Hide();
                    cnntest.ShowDialog();
                    cnntest = null;
                    this.Show();
                }
                else
                {
                    var test = new Test();
                    this.Hide();
                    test.ShowDialog();
                    test = null;
                    this.Show();
                }
                
            }
            catch
            {
                this.Show();
                Console.WriteLine("fuck");
            }
        }

       

        private void bunifuSlider1_DragOver(object sender, DragEventArgs e)
        {

        }

        private void bunifuSlider1_ValueChangeComplete(object sender, EventArgs e)
        {
            if (bunifuSlider1.Value > 30)
            {
                // if (proceed == 0)
                //  {
                var testsize = new Testsize();
                this.Hide();
                testsize.ShowDialog();
                testsize = null;
                this.Show();

                Console.WriteLine("value bada");
                // proceed = 1;
                // return;
                // }
                //  else
                // {
                // proceed = 1;
                //  return;
                // }

            }
            else
            {
                Console.WriteLine("less than 30");
            }
        }

        private void bunifuSlider1_MouseClick(object sender, MouseEventArgs e)
        {
            
        }

        private void IC_OnChange(object sender, EventArgs e)
        {
          
        }

        private void IC_MouseClick(object sender, MouseEventArgs e)
        {
            if (IC.Checked)
            {
                LinearRegression.Checked = false;
                SGD.Checked = false;
                DTR.Checked = false;
                RFR.Checked = false;
                RFC.Checked = false;
                DTC.Checked = false;
                Perceptron.Checked = false;
                run.ActiveFillColor = Color.DodgerBlue;
                run.ActiveLineColor = Color.DodgerBlue;
                run.IdleForecolor = Color.DodgerBlue;
                run.IdleLineColor = Color.DodgerBlue;
            }
        }

        private void confusionmatbox_Click(object sender, EventArgs e)
        {
            
        }

        private void Generate_Click(object sender, EventArgs e)
        {
            create_pdfreport();
            try
            {
                System.IO.File.Delete(Application.StartupPath + @"/Pdfs/ML_report.pdf");
                Directory.Delete(Application.StartupPath + @"/Pdfs");
            }
            catch
            {

            }
            
            ProcessStartInfo start = new ProcessStartInfo();
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.CreateNoWindow = true;
            start.UseShellExecute = false;
            start.FileName = "cmd.exe";
            start.Arguments = string.Format(@"/C "+ pypath +  " " + Application.StartupPath + @"/txttopdf.py");
            start.RedirectStandardOutput = true;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    string pdf = reader.ReadToEnd();
                    Console.WriteLine(pdf);                 
                    MessageBox.Show("Pdf file generated and will be save on your Desktop Folder");
                } 
            }



            System.IO.File.Delete(Application.StartupPath + @"/txttopdf.py");
            System.IO.File.Delete(Application.StartupPath + @"/Documents/ML.docx");
            Directory.Delete(Application.StartupPath + @"/Documents");

            //System.Windows.Forms.FolderBrowserDialog folderdialog = new System.Windows.Forms.FolderBrowserDialog();
            //if (folderdialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    string folderpath = folderdialog.SelectedPath;
            //    Console.WriteLine(folderpath);
            //    System.IO.File.Copy(Application.StartupPath + @"/Pdfs/ML_report.pdf", folderpath+@"/ML_report.pdf");
            //}
            try
            {
                System.IO.File.Copy(Application.StartupPath + @"/Pdfs/ML_report.pdf", Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"/ML_report.pdf",true);
            }
            catch
            {
                Console.WriteLine("pdf exists");
            }
            // System.IO.File.Copy(Application.StartupPath + @"/Pdfs/ML_report.pdf",);
        }

        private void bunifuSlider1_MouseEnter(object sender, EventArgs e)
        {
            

        }

        

        private void Form1_Activated(object sender, EventArgs e)
        {
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            //GC.Collect();
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void inputpanel1_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                System.IntPtr ptr = CreateRoundRectRgn(0, 0, inputpanel1.Width, inputpanel1.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
                inputpanel1.Region = System.Drawing.Region.FromHrgn(ptr);
            }
            catch
            {

            }
        }

        private void outputpanel1_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                System.IntPtr ptr = CreateRoundRectRgn(0, 0, outputpanel1.Width, outputpanel1.Height, 30, 30); // _BoarderRaduis can be adjusted to your needs, try 15 to start.
                outputpanel1.Region = System.Drawing.Region.FromHrgn(ptr);
            }
            catch
            {

            }
        }

       
        public void chkcheckboxes(object sender, EventArgs e)
        {
            for (int i = 0; i < mystring.Length; i++)
            {
                if (icheckboxlist[i].Checked)
                {
                    ocheckboxlist[i].Checked = false;
                }
                else if (ocheckboxlist[i].Checked)
                {
                    icheckboxlist[i].Checked = false;
                }

            }
        }
     
        public void createcheckboxes1()
        {
            int ypos = 10;
            outputpanel1.HorizontalScroll.Maximum = 0;
            outputpanel1.AutoScroll = false;
            outputpanel1.VerticalScroll.Visible = false;
            outputpanel1.AutoScroll = true;
            //List<string> lstArray = mystring.ToList();
            for (int i = 0; i < mystring.Length; i++)
            {
                ocheckboxlist[i] = new Bunifu.Framework.UI.BunifuCheckbox();
                ocheckboxlist[i].Location = new Point(20, ypos);
                ocheckboxlist[i].Checked = false;
                ocheckboxlist[i].BackColor = Color.White;
                ocheckboxlist[i].ForeColor = Color.White;
                ocheckboxlist[i].ChechedOffColor = Color.White;
                ocheckboxlist[i].CheckedOnColor = Color.CornflowerBlue;
                ocheckboxlist[i].Name = Convert.ToString(i);
                //checkboxlist[i].


                for (int j = 0; j < mystring.Length; j++)
                {
                    labellist[j] = new Label();
                    labellist[j].Location = new Point(50, ypos);
                    labellist[j].Text = columns[i];
                    labellist[j].Size = new System.Drawing.Size(300, 22);
                    labellist[j].Font = new System.Drawing.Font("Segoe UI", 12.0F, FontStyle.Regular);
                    labellist[j].ForeColor = Color.Black;
                    //Console.WriteLine(columns[i]);
                }




                outputpanel1.Controls.Add(ocheckboxlist[i]);
                outputpanel1.Controls.Add(labellist[i]);
                //outputpanel1.Controls.Add(checkboxlist[i]);
                //outputpanel1.Controls.Add(checkboxlist[i]);

                ypos += 30;
                ocheckboxlist[i].OnChange += new EventHandler(chkcheckboxes);
            }

          
        }

        public void createcheckboxes()
        {
            int ypos = 10;
            inputpanel1.HorizontalScroll.Maximum = 0;
            inputpanel1.AutoScroll = false;
            inputpanel1.VerticalScroll.Visible = false;
            inputpanel1.AutoScroll = true;
            //List<string> lstArray = mystring.ToList();
            for (int i = 0; i < mystring.Length; i++)
            {
                icheckboxlist[i] = new Bunifu.Framework.UI.BunifuCheckbox();
                icheckboxlist[i].Location = new Point(20, ypos);
                icheckboxlist[i].Checked = false;
                icheckboxlist[i].BackColor = Color.White;
                icheckboxlist[i].ForeColor = Color.White;
                icheckboxlist[i].ChechedOffColor = Color.White;
                icheckboxlist[i].CheckedOnColor = Color.CornflowerBlue;
                icheckboxlist[i].Name =Convert.ToString(i);


                for (int j = 0; j < mystring.Length; j++)
                {
                    labellist[j] = new Label();
                    labellist[j].Location = new Point(50, ypos);
                    labellist[j].Text = columns[i];
                    labellist[j].Size = new System.Drawing.Size(300, 22);
                    labellist[j].Font = new System.Drawing.Font("Segoe UI", 12.0F, FontStyle.Regular);
                    labellist[j].ForeColor = Color.Black;
                    //Console.WriteLine(columns[i]);
                }




                inputpanel1.Controls.Add(icheckboxlist[i]);
                inputpanel1.Controls.Add(labellist[i]);

                ypos += 30;
                icheckboxlist[i].OnChange += new EventHandler(chkcheckboxes);
            }

           

        }
        public void create_OLS()
        {
            string default_OLS = @"import pickle
import sys
import getopt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

def main(argv):
    datafile = """"
    inputs = []
    outputs = []

    try:
        opts, args = getopt.getopt(argv,""hd: i: o: s: "",[""dfile = "",""ifile = "",""ofile = "",""sfile = ""])
    except getopt.GetoptError:
        print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>(getopterror)"")
        sys.exit(2)
    for opt, arg in opts:
        if opt == ""-d"":
            datafile = arg
        elif opt in (""-i"", ""--ifile""):
            inputs.append(arg)
        elif opt in (""-o"", ""--ofile""):
            outputs.append(arg)
        elif opt in (""-s"", ""--sfile""):
            test_size = arg
        else:
            print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>"")
            sys.exit()

    df = pd.read_csv(datafile[1:])
    df = df.dropna()
    X_train, X_test, y_train, y_test = train_test_split(df[inputs[0].split(',')], df[outputs], test_size = float(test_size))
    lr = LinearRegression()
    lr_model = lr.fit(X_train, y_train)
    print(""Accuracy: "", round(lr.score(X_test, y_test) * 100, 2))

    pkl_filename =""LinearRegression.pkl""
    with open(pkl_filename,'wb') as file:
        pickle.dump(lr_model, file)

if __name__ == ""__main__"":
    main(sys.argv[1:])
";

            string from = Application.StartupPath+"/LinearRegression.py";
            using (StreamWriter file = new StreamWriter(from))
            {
                
                file.WriteLine(default_OLS);
                
            }

        }


        public void create_SGD()
        {
            string default_sgd = @"import pickle
import sys
import getopt
import numpy as np
import pandas as pd
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

def main(argv):
    datafile = """"
    inputs = []
    outputs = []

    try:
        opts, args = getopt.getopt(argv,""hd: i: o: s: "",[""dfile = "",""ifile = "",""ofile = "",""sfile = ""])
    except getopt.GetoptError:
        print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>(getopterror)"")
        sys.exit(2)
    for opt, arg in opts:
        if opt == ""-d"":
            datafile = arg
        elif opt in (""-i"", ""--ifile""):
            inputs.append(arg)
        elif opt in (""-o"", ""--ofile""):
            outputs.append(arg)
        elif opt in (""-s"", ""--sfile""):
            test_size = arg
        else:
            print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>"")
            sys.exit()

    df = pd.read_csv(datafile[1:])
    df = df.dropna()
    X_train, X_test, y_train, y_test = train_test_split(df[inputs[0].split(',')], df[outputs], test_size = float(test_size))
    sgd = linear_model.SGDRegressor(max_iter = 1000, tol = 1e-3)
    sgd_model = sgd.fit(X_train, np.ravel(y_train))
    print(""Accuracy: "", round(sgd.score(X_test, y_test) * 100, 2))

    pkl_filename = ""SGD.pkl""
    with open(pkl_filename,'wb') as file:
        pickle.dump(sgd_model, file)

if __name__ == ""__main__"":
    main(sys.argv[1:])";



            string from = Application.StartupPath + "/SGD.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_sgd);

            }
        }

        public void create_DTR()
        {
            string default_dtr = @"import pickle
import sys
import getopt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error

def main(argv):
    datafile = """"
    inputs = []
    outputs = []

    try:
        opts, args = getopt.getopt(argv,""hd: i: o: s: "",[""dfile = "",""ifile = "",""ofile = "",""sfile = ""])
    except getopt.GetoptError:
        print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>(getopterror)"")
        sys.exit(2)
    for opt, arg in opts:
        if opt == ""-d"":
            datafile = arg
        elif opt in (""-i"", ""--ifile""):
            inputs.append(arg)
        elif opt in (""-o"", ""--ofile""):
            outputs.append(arg)
        elif opt in (""-s"", ""--sfile""):
            test_size = arg
        else:
            print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>"")
            sys.exit()

    df = pd.read_csv(datafile[1:])
    df = df.dropna()
    X_train, X_test, y_train, y_test = train_test_split(df[inputs[0].split(',')], df[outputs], test_size = float(test_size))
    dr = DecisionTreeRegressor()
    dr_model = dr.fit(X_train, y_train)
    print(""Accuracy: "", round(dr.score(X_test, y_test) * 100, 2))

    pkl_filename = ""DTR.pkl""
    with open(pkl_filename,'wb') as file:
        pickle.dump(dr_model, file)

if __name__ == ""__main__"":
    main(sys.argv[1:])
";

            string from = Application.StartupPath + "/DTR.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_dtr);

            }
        }


        public void create_RFR()
        {
            String default_rfr = @"import pickle
import sys
import getopt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error

def main(argv):
    datafile = """"
    inputs = []
    outputs = []

    try:
        opts, args = getopt.getopt(argv,""hd: i: o: s: "",[""dfile = "",""ifile = "",""ofile = "",""sfile = ""])
    except getopt.GetoptError:
        print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>(getopterror)"")
        sys.exit(2)
    for opt, arg in opts:
        if opt == ""-d"":
            datafile = arg
        elif opt in (""-i"", ""--ifile""):
            inputs.append(arg)
        elif opt in (""-o"", ""--ofile""):
            outputs.append(arg)
        elif opt in (""-s"", ""--sfile""):
            test_size = arg
        else:
            print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>"")
            sys.exit()

    df = pd.read_csv(datafile[1:])
    df = df.dropna()
    X_train, X_test, y_train, y_test = train_test_split(df[inputs[0].split(',')], df[outputs], test_size = float(test_size))
    rf = RandomForestRegressor(n_estimators = 100)
    rf_model = rf.fit(X_train, np.array(y_train).ravel())
    print(""Accuracy: "", round(rf.score(X_test, y_test) * 100, 2))

    pkl_filename = ""RFR.pkl""
    with open(pkl_filename,'wb') as file:
        pickle.dump(rf_model, file)

if __name__ == ""__main__"":
    main(sys.argv[1:])
";


            string from = Application.StartupPath + "/RFR.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_rfr);

            }
        }

        public void create_Perceptron()
        {
            string default_per = @"import pickle
import sys
import getopt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import Perceptron
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sb

def main(argv):
    datafile = """"
    inputs = []
    outputs = []

    try:
        opts, args = getopt.getopt(argv,""hd: i: o: s: "",[""dfile = "",""ifile = "",""ofile = "",""sfile = ""])
    except getopt.GetoptError:
        print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>(getopterror)"")
        sys.exit(2)
    for opt, arg in opts:
        if opt == ""-d"":
            datafile = arg
        elif opt in (""-i"", ""--ifile""):
            inputs.append(arg)
        elif opt in (""-o"", ""--ofile""):
            outputs.append(arg)
        elif opt in (""-s"", ""--sfile""):
            test_size = arg
        else:
            print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>"")
            sys.exit()

    df = pd.read_csv(datafile[1:])
    df = df.dropna()
    X_train, X_test, y_train, y_test = train_test_split(df[inputs[0].split(',')], df[outputs], test_size = float(test_size))
    per = Perceptron(max_iter = 1000, tol = 1e-3)
    per_model = per.fit(X_train, np.array(y_train).ravel())
    print(""Accuracy: "", round(per.score(X_test, y_test) * 100, 2))
    fig, ax = plt.subplots()
    matimage = sb.heatmap(confusion_matrix(y_test, per_model.predict(X_test)), annot = True, cmap = ""Blues"")
    ax.set_yticklabels(np.unique(y_test), va = 'center')
    ax.set_xticklabels(np.unique(y_test), ha = 'center')
    fig = matimage.get_figure()
    fig.savefig('Perceptron_confusion.png',bbox_inches = 'tight', dpi = 400)
    pkl_filename = ""Perceptron.pkl""
    with open(pkl_filename,'wb') as file:
        pickle.dump(per_model, file)

if __name__ == ""__main__"":
    main(sys.argv[1:])
";

            string from = Application.StartupPath + "/Perceptron.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_per);

            }
        }


        public void create_DTC()
        {

            string default_dtc = @"import pickle
import sys
import getopt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix
import seaborn as sb
import matplotlib.pyplot as plt


def main(argv):
    datafile = """"
    inputs = []
    outputs = []

    try:
        opts, args = getopt.getopt(argv,""hd: i: o: s: "",[""dfile = "",""ifile = "",""ofile = "",""sfile = ""])
    except getopt.GetoptError:
        print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>(getopterror)"")
        sys.exit(2)
    for opt, arg in opts:
        if opt == ""-d"":
            datafile = arg
        elif opt in (""-i"", ""--ifile""):
            inputs.append(arg)
        elif opt in (""-o"", ""--ofile""):
            outputs.append(arg)
        elif opt in (""-s"", ""--sfile""):
            test_size = arg
        else:
            print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>"")
            sys.exit()

    df = pd.read_csv(datafile[1:])
    df = df.dropna()
    X_train, X_test, y_train, y_test = train_test_split(df[inputs[0].split(',')], df[outputs], test_size = float(test_size))
    dc = DecisionTreeClassifier()
    dc_model = dc.fit(X_train, np.array(y_train).ravel())
    print(""Accuracy: "", round(dc.score(X_test, y_test) * 100, 2))

    fig, ax = plt.subplots()
    matimage = sb.heatmap(confusion_matrix(y_test, dc_model.predict(X_test)), annot = True, cmap = ""Blues"")
    ax.set_yticklabels(np.unique(y_test), va = 'center')
    ax.set_xticklabels(np.unique(y_test), ha = 'center')
    fig = matimage.get_figure()
    fig.savefig('DTC_confusion.png',bbox_inches = 'tight', dpi = 400)

    pkl_filename = ""DTC.pkl""
    with open(pkl_filename,'wb') as file:
        pickle.dump(dc_model, file)

if __name__ == ""__main__"":
    main(sys.argv[1:])
";

            string from = Application.StartupPath + "/DTC.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_dtc);

            }
        }

        public void create_RFC()
        {
            string default_RFC = @"import pickle
import sys
import getopt
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sb

def main(argv):
    datafile = """"
    inputs = []
    outputs = []

    try:
        opts, args = getopt.getopt(argv,""hd: i: o: s: "",[""dfile = "",""ifile = "",""ofile = "",""sfile = ""])
    except getopt.GetoptError:
        print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>(getopterror)"")
        sys.exit(2)
    for opt, arg in opts:
        if opt == ""-d"":
            datafile = arg
        elif opt in (""-i"", ""--ifile""):
            inputs.append(arg)
        elif opt in (""-o"", ""--ofile""):
            outputs.append(arg)
        elif opt in (""-s"", ""--sfile""):
            test_size = arg
        else:
            print(""Pythonfile.py -d <datafile> -i <inputcolumns> -o <outputcolumns> -s <train-test-split>"")
            sys.exit()

    df = pd.read_csv(datafile[1:])
    df = df.dropna()
    X_train, X_test, y_train, y_test = train_test_split(df[inputs[0].split(',')], df[outputs], test_size = float(test_size))
    rfc = RandomForestClassifier(n_estimators = 1000)
    rfc_model = rfc.fit(X_train, np.array(y_train).ravel())
    print(""Accuracy: "", round(rfc.score(X_test, y_test) * 100, 2))
    fig, ax = plt.subplots()
    matimage = sb.heatmap(confusion_matrix(y_test, rfc_model.predict(X_test)), annot = True, cmap = ""Blues"")
    ax.set_yticklabels(np.unique(y_test), va = 'center')
    ax.set_xticklabels(np.unique(y_test), ha = 'center')
    fig = matimage.get_figure()
    fig.savefig('RFC_confusion.png',bbox_inches = 'tight', dpi = 400)
    pkl_filename = ""RFC.pkl""
    with open(pkl_filename,'wb') as file:
        pickle.dump(rfc_model, file)

if __name__ == ""__main__"":
    main(sys.argv[1:])
";

            string from = Application.StartupPath + "/RFC.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_RFC);

            }
        }

        public void create_IC()
        {
            float split = Convert.ToInt32(label5.Text);
            string default_IC = @"import zipfile 
import os
from distutils.dir_util import copy_tree
import numpy as np
import pandas as pd 
from keras.preprocessing.image import ImageDataGenerator, load_img
from keras.utils import to_categorical
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import random
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D, Dropout, Flatten, Dense, Activation, BatchNormalization
from keras.callbacks import EarlyStopping, ReduceLROnPlateau
from keras.models import load_model
from keras.preprocessing import image
import shutil


list1=[]
destpath = """"
copypath = """"
os.mkdir(r"""+ Application.StartupPath + @"\root"")
os.mkdir(r""" + Application.StartupPath + @"\root\mix"")
path = r""" + Application.StartupPath + @"\root\mix""
categories = []
IMAGE_WIDTH = 128
IMAGE_HEIGHT = 128
IMAGE_SIZE = (IMAGE_WIDTH, IMAGE_HEIGHT)
IMAGE_CHANNELS = 3

for root_fol, directory_fol, files in os.walk(r"""+ Application.StartupPath +@""") : 
        for file in files : 
            if file.endswith('.zip'):
                zzip = zipfile.ZipFile(r"""+Application.StartupPath+@""" + ""\\"" + file)
                zzip.extractall(r"""+Application.StartupPath +@"\root"")
                zzip.close()


for root, dirs, files in os.walk(r"""+Application.StartupPath+@"\root"", topdown = False):
    for name in dirs:
        list1.append(os.path.basename(os.path.join(root, name)))

cnt = 0
for root, dirs, files in os.walk(r"""+Application.StartupPath+ @"\root""):
    for name in dirs:
        destpath = os.path.basename(os.path.join(root, name))
        print(destpath)
        if name != 'mix':
            for images in os.listdir(os.path.join(root, name)):
                print(images + "" = image"")
                cnt = cnt + 1
                copypath = ""./root/mix/""+ destpath + ""."" + str(cnt) + "".jpg""
                print(copypath)
                print(os.path.join(root, name)+ ""/"" + images)
                os.rename(os.path.join(root, name) + ""\\"" + images, copypath)

filenames = os.listdir(path)
for filename in filenames:
    category = filename.split('.')[0]
    if category in list1:
            categories.append(str(list1.index(category))) #str(list1.index(category))


df = pd.DataFrame({
    'filename': filenames,
    'category': categories})

#-----------------------------------------------------------------------------------------------------------#

model = Sequential()

model.add(Conv2D(32, (3, 3), activation = 'relu', input_shape = (IMAGE_WIDTH, IMAGE_HEIGHT, IMAGE_CHANNELS)))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3, 3), activation = 'relu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Dropout(0.25))

model.add(Conv2D(128, (3, 3), activation = 'relu'))
model.add(BatchNormalization())
model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Dropout(0.25))

model.add(Flatten())
model.add(Dense(512, activation = 'relu'))
model.add(BatchNormalization())
model.add(Dropout(0.5))
model.add(Dense(len(list1[:-1]), activation = 'softmax')) # 2 because we have cat and dog classes

model.compile(loss = 'categorical_crossentropy', optimizer = 'rmsprop', metrics =['accuracy'])

model.summary()

#-----------------------------------------------------------------------------------------------------------#


earlystop = EarlyStopping(patience = 10)
learning_rate_reduction = ReduceLROnPlateau(monitor = 'val_acc',
                                            patience = 2,
                                            verbose = 1,
                                            factor = 0.5,
                                            min_lr = 0.00001)
callbacks = [earlystop, learning_rate_reduction]

train_df, validate_df = train_test_split(df, test_size = " + split/100+ @")
train_df = train_df.reset_index(drop = True)
validate_df = validate_df.reset_index(drop = True)

total_train = train_df.shape[0]
total_validate = validate_df.shape[0]
batch_size = 15

train_datagen = ImageDataGenerator(
    rotation_range = 15,
    rescale = 1./ 255,
    shear_range = 0.1,
    zoom_range = 0.2,
    horizontal_flip = True,
    width_shift_range = 0.1,
    height_shift_range = 0.1)

train_generator = train_datagen.flow_from_dataframe(
    train_df,
    path,
    x_col = 'filename',
    y_col = 'category',
    target_size = IMAGE_SIZE,
    class_mode = 'categorical',
    batch_size = batch_size)


validation_datagen = ImageDataGenerator(rescale = 1./ 255)
validation_generator = validation_datagen.flow_from_dataframe(
    validate_df,
    path,
    x_col = 'filename',
    y_col = 'category',
    target_size = IMAGE_SIZE,
    class_mode = 'categorical',
    batch_size = batch_size)


epochs = 10
history = model.fit_generator(
    train_generator,
    epochs = epochs,
    validation_data = validation_generator,
    validation_steps = total_validate//batch_size,
    steps_per_epoch = total_train//batch_size,
    callbacks = callbacks)

count = 1
print(""model_accuracy"")
for i in history.history['val_accuracy']:
    if(count==20):
        print(""Epoch: "" + str(count), "" "", ""Accuracy: "", round(i * 100, 2))
        break
    print(""Epoch: "" + str(count), ""   "", ""Accuracy: "", round(i * 100, 2))
    count = count + 1

model.save_weights(""cnn.h5"")

with open('cnn.json', 'w') as f:
    f.write(model.to_json())
    f.close()

shutil.rmtree(""./root/mix"")";

            string from = Application.StartupPath + "/IC.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_IC);

            }
        }


        public void create_pdfreport()
        {
            string default_pdf = @"from docx import Document
from docx.enum.text import WD_UNDERLINE
import time
import os
import sys
import comtypes.client
import glob


wordpath           = r'Documents'
pdfpath            = r'Pdfs'
outputdata = """"


txtfile = open(r"""+Application.StartupPath+@"/Outputhistory.txt"+@""",'r')
outputdata = txtfile.read()
txtfile.close()


def ReplacePrefix(document, prefixdata, toreplace):
    for paragraph in document.paragraphs:
        if toreplace in paragraph.text:
            inline = paragraph.runs
            for i in range(len(inline)):
                if toreplace in inline[i].text:
                    text = inline[i].text.replace(toreplace, ""{0}"".format(prefixdata))
                    inline[i].text = text

os.mkdir(os.path.abspath(wordpath))
os.mkdir(os.path.abspath(pdfpath))

document = Document(r"""+Application.StartupPath+@"/Eduvance_ML_report.docx"+@""")

ReplacePrefix(document, outputdata, ""!!"")

temppath = r""./Documents/ML.docx""
document.save(temppath)
time.sleep(2)

wdFormatPDF = 17
try:
    infile = (glob.glob(""%s\\*.docx"" % (wordpath)))
    word = comtypes.client.CreateObject('word.Application')
    for in_file in infile: 
        out_file = os.path.abspath(r""./Pdfs/ML_report.pdf"")
        doc = word.Documents.Open(os.path.abspath(in_file))
        doc.SaveAs(out_file, FileFormat = wdFormatPDF)
        doc.Close()
        print(""{0} file done"".format(in_file))
    word.Quit()
    time.sleep(2)
except Exception as e:
    print(e)
    doc.Close()
    word.Quit()";

            string from = Application.StartupPath + "/txttopdf.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_pdf);

            }
        }
        public void create_libinstall()
        {
            string default_install = @"import os 
os.system(r'"+ pypath.Substring(0, 54) + @"Scripts\pip3.6.exe" + @" install python-docx comtypes numpy matplotlib pandas sklearn seaborn tensorflow keras pillow')";

            string from = Application.StartupPath + "/Libinstall.py";
            using (StreamWriter file = new StreamWriter(from))
            {

                file.WriteLine(default_install);

            }
        }
    }
}
