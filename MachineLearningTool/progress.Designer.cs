﻿namespace MachineLearningTool
{
    partial class progress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(progress));
            this.Initiate = new System.Windows.Forms.Label();
            this.datareading = new System.Windows.Forms.Label();
            this.modelbuilding = new System.Windows.Forms.Label();
            this.finished = new System.Windows.Forms.Label();
            this.bunifuProgressBar1 = new Bunifu.Framework.UI.BunifuProgressBar();
            this.bunifuProgressBar2 = new Bunifu.Framework.UI.BunifuProgressBar();
            this.bunifuProgressBar3 = new Bunifu.Framework.UI.BunifuProgressBar();
            this.inittick = new System.Windows.Forms.PictureBox();
            this.finishing = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.modelbuild = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.dataread = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.Init = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.datareadtick = new System.Windows.Forms.PictureBox();
            this.modelbuildtick = new System.Windows.Forms.PictureBox();
            this.finishtick = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.inittick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.datareadtick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelbuildtick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.finishtick)).BeginInit();
            this.SuspendLayout();
            // 
            // Initiate
            // 
            this.Initiate.AutoSize = true;
            this.Initiate.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Initiate.ForeColor = System.Drawing.Color.SteelBlue;
            this.Initiate.Location = new System.Drawing.Point(97, 350);
            this.Initiate.Name = "Initiate";
            this.Initiate.Size = new System.Drawing.Size(143, 38);
            this.Initiate.TabIndex = 1;
            this.Initiate.Text = "Initializing";
            // 
            // datareading
            // 
            this.datareading.AutoSize = true;
            this.datareading.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datareading.ForeColor = System.Drawing.Color.SteelBlue;
            this.datareading.Location = new System.Drawing.Point(341, 350);
            this.datareading.Name = "datareading";
            this.datareading.Size = new System.Drawing.Size(182, 38);
            this.datareading.TabIndex = 5;
            this.datareading.Text = "Data Reading";
            // 
            // modelbuilding
            // 
            this.modelbuilding.AutoSize = true;
            this.modelbuilding.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modelbuilding.ForeColor = System.Drawing.Color.SteelBlue;
            this.modelbuilding.Location = new System.Drawing.Point(610, 350);
            this.modelbuilding.Name = "modelbuilding";
            this.modelbuilding.Size = new System.Drawing.Size(205, 38);
            this.modelbuilding.TabIndex = 6;
            this.modelbuilding.Text = "Model Building";
            // 
            // finished
            // 
            this.finished.AutoSize = true;
            this.finished.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.finished.ForeColor = System.Drawing.Color.SteelBlue;
            this.finished.Location = new System.Drawing.Point(908, 350);
            this.finished.Name = "finished";
            this.finished.Size = new System.Drawing.Size(128, 38);
            this.finished.TabIndex = 7;
            this.finished.Text = "Finishing";
            // 
            // bunifuProgressBar1
            // 
            this.bunifuProgressBar1.BackColor = System.Drawing.Color.Silver;
            this.bunifuProgressBar1.BorderRadius = 5;
            this.bunifuProgressBar1.Location = new System.Drawing.Point(222, 256);
            this.bunifuProgressBar1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuProgressBar1.MaximumValue = 100;
            this.bunifuProgressBar1.Name = "bunifuProgressBar1";
            this.bunifuProgressBar1.ProgressColor = System.Drawing.Color.SeaGreen;
            this.bunifuProgressBar1.Size = new System.Drawing.Size(163, 10);
            this.bunifuProgressBar1.TabIndex = 8;
            this.bunifuProgressBar1.Value = 0;
            // 
            // bunifuProgressBar2
            // 
            this.bunifuProgressBar2.BackColor = System.Drawing.Color.Silver;
            this.bunifuProgressBar2.BorderRadius = 5;
            this.bunifuProgressBar2.Location = new System.Drawing.Point(490, 256);
            this.bunifuProgressBar2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuProgressBar2.MaximumValue = 100;
            this.bunifuProgressBar2.Name = "bunifuProgressBar2";
            this.bunifuProgressBar2.ProgressColor = System.Drawing.Color.SeaGreen;
            this.bunifuProgressBar2.Size = new System.Drawing.Size(164, 10);
            this.bunifuProgressBar2.TabIndex = 9;
            this.bunifuProgressBar2.Value = 0;
            this.bunifuProgressBar2.progressChanged += new System.EventHandler(this.bunifuProgressBar2_progressChanged);
            // 
            // bunifuProgressBar3
            // 
            this.bunifuProgressBar3.BackColor = System.Drawing.Color.Silver;
            this.bunifuProgressBar3.BorderRadius = 5;
            this.bunifuProgressBar3.Location = new System.Drawing.Point(761, 256);
            this.bunifuProgressBar3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuProgressBar3.MaximumValue = 100;
            this.bunifuProgressBar3.Name = "bunifuProgressBar3";
            this.bunifuProgressBar3.ProgressColor = System.Drawing.Color.SeaGreen;
            this.bunifuProgressBar3.Size = new System.Drawing.Size(164, 10);
            this.bunifuProgressBar3.TabIndex = 10;
            this.bunifuProgressBar3.Value = 0;
            // 
            // inittick
            // 
            this.inittick.Image = global::MachineLearningTool.Properties.Resources.tickmark;
            this.inittick.Location = new System.Drawing.Point(104, 196);
            this.inittick.Name = "inittick";
            this.inittick.Size = new System.Drawing.Size(119, 117);
            this.inittick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.inittick.TabIndex = 12;
            this.inittick.TabStop = false;
            // 
            // finishing
            // 
            this.finishing.animated = false;
            this.finishing.animationIterval = 5;
            this.finishing.animationSpeed = 1;
            this.finishing.BackColor = System.Drawing.Color.Transparent;
            this.finishing.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("finishing.BackgroundImage")));
            this.finishing.Enabled = false;
            this.finishing.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.finishing.ForeColor = System.Drawing.Color.DodgerBlue;
            this.finishing.LabelVisible = false;
            this.finishing.LineProgressThickness = 8;
            this.finishing.LineThickness = 5;
            this.finishing.Location = new System.Drawing.Point(913, 198);
            this.finishing.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.finishing.MaxValue = 100;
            this.finishing.Name = "finishing";
            this.finishing.ProgressBackColor = System.Drawing.Color.Transparent;
            this.finishing.ProgressColor = System.Drawing.Color.SteelBlue;
            this.finishing.Size = new System.Drawing.Size(117, 117);
            this.finishing.TabIndex = 4;
            this.finishing.Value = 60;
            // 
            // modelbuild
            // 
            this.modelbuild.animated = false;
            this.modelbuild.animationIterval = 5;
            this.modelbuild.animationSpeed = 1;
            this.modelbuild.BackColor = System.Drawing.Color.Transparent;
            this.modelbuild.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("modelbuild.BackgroundImage")));
            this.modelbuild.Enabled = false;
            this.modelbuild.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.modelbuild.ForeColor = System.Drawing.Color.DodgerBlue;
            this.modelbuild.LabelVisible = false;
            this.modelbuild.LineProgressThickness = 8;
            this.modelbuild.LineThickness = 5;
            this.modelbuild.Location = new System.Drawing.Point(642, 196);
            this.modelbuild.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.modelbuild.MaxValue = 100;
            this.modelbuild.Name = "modelbuild";
            this.modelbuild.ProgressBackColor = System.Drawing.Color.Transparent;
            this.modelbuild.ProgressColor = System.Drawing.Color.SteelBlue;
            this.modelbuild.Size = new System.Drawing.Size(117, 117);
            this.modelbuild.TabIndex = 3;
            this.modelbuild.Value = 60;
            // 
            // dataread
            // 
            this.dataread.animated = false;
            this.dataread.animationIterval = 5;
            this.dataread.animationSpeed = 1;
            this.dataread.BackColor = System.Drawing.Color.Transparent;
            this.dataread.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("dataread.BackgroundImage")));
            this.dataread.Enabled = false;
            this.dataread.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.dataread.ForeColor = System.Drawing.Color.DodgerBlue;
            this.dataread.LabelVisible = false;
            this.dataread.LineProgressThickness = 8;
            this.dataread.LineThickness = 5;
            this.dataread.Location = new System.Drawing.Point(372, 196);
            this.dataread.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.dataread.MaxValue = 100;
            this.dataread.Name = "dataread";
            this.dataread.ProgressBackColor = System.Drawing.Color.Transparent;
            this.dataread.ProgressColor = System.Drawing.Color.SteelBlue;
            this.dataread.Size = new System.Drawing.Size(117, 117);
            this.dataread.TabIndex = 2;
            this.dataread.Value = 60;
            // 
            // Init
            // 
            this.Init.animated = true;
            this.Init.animationIterval = 5;
            this.Init.animationSpeed = 1;
            this.Init.BackColor = System.Drawing.Color.Transparent;
            this.Init.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Init.BackgroundImage")));
            this.Init.Enabled = false;
            this.Init.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.Init.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Init.LabelVisible = false;
            this.Init.LineProgressThickness = 8;
            this.Init.LineThickness = 5;
            this.Init.Location = new System.Drawing.Point(104, 197);
            this.Init.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.Init.MaxValue = 100;
            this.Init.Name = "Init";
            this.Init.ProgressBackColor = System.Drawing.Color.Transparent;
            this.Init.ProgressColor = System.Drawing.Color.SteelBlue;
            this.Init.Size = new System.Drawing.Size(117, 117);
            this.Init.TabIndex = 0;
            this.Init.Value = 60;
            // 
            // datareadtick
            // 
            this.datareadtick.Image = global::MachineLearningTool.Properties.Resources.tickmark;
            this.datareadtick.Location = new System.Drawing.Point(372, 198);
            this.datareadtick.Name = "datareadtick";
            this.datareadtick.Size = new System.Drawing.Size(119, 117);
            this.datareadtick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.datareadtick.TabIndex = 13;
            this.datareadtick.TabStop = false;
            // 
            // modelbuildtick
            // 
            this.modelbuildtick.Image = global::MachineLearningTool.Properties.Resources.tickmark;
            this.modelbuildtick.Location = new System.Drawing.Point(642, 198);
            this.modelbuildtick.Name = "modelbuildtick";
            this.modelbuildtick.Size = new System.Drawing.Size(119, 117);
            this.modelbuildtick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.modelbuildtick.TabIndex = 14;
            this.modelbuildtick.TabStop = false;
            // 
            // finishtick
            // 
            this.finishtick.Image = global::MachineLearningTool.Properties.Resources.tickmark;
            this.finishtick.Location = new System.Drawing.Point(913, 198);
            this.finishtick.Name = "finishtick";
            this.finishtick.Size = new System.Drawing.Size(119, 117);
            this.finishtick.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.finishtick.TabIndex = 15;
            this.finishtick.TabStop = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(136, 245);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 23);
            this.label4.TabIndex = 16;
            this.label4.Text = "Step 1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(406, 245);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 23);
            this.label5.TabIndex = 17;
            this.label5.Text = "Step 2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(674, 247);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 23);
            this.label1.TabIndex = 18;
            this.label1.Text = "Step 3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(945, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 23);
            this.label2.TabIndex = 19;
            this.label2.Text = "Step 4";
            // 
            // progress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(1140, 575);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.finishtick);
            this.Controls.Add(this.modelbuildtick);
            this.Controls.Add(this.datareadtick);
            this.Controls.Add(this.inittick);
            this.Controls.Add(this.bunifuProgressBar3);
            this.Controls.Add(this.bunifuProgressBar2);
            this.Controls.Add(this.bunifuProgressBar1);
            this.Controls.Add(this.finished);
            this.Controls.Add(this.modelbuilding);
            this.Controls.Add(this.datareading);
            this.Controls.Add(this.finishing);
            this.Controls.Add(this.modelbuild);
            this.Controls.Add(this.dataread);
            this.Controls.Add(this.Initiate);
            this.Controls.Add(this.Init);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "progress";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "progress";
            this.Activated += new System.EventHandler(this.progress_Activated);
            this.Load += new System.EventHandler(this.progress_Load);
            this.VisibleChanged += new System.EventHandler(this.progress_VisibleChanged);
            ((System.ComponentModel.ISupportInitialize)(this.inittick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.datareadtick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.modelbuildtick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.finishtick)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Bunifu.Framework.UI.BunifuCircleProgressbar Init;
        private System.Windows.Forms.Label Initiate;
        private Bunifu.Framework.UI.BunifuCircleProgressbar dataread;
        private Bunifu.Framework.UI.BunifuCircleProgressbar modelbuild;
        private Bunifu.Framework.UI.BunifuCircleProgressbar finishing;
        private System.Windows.Forms.Label datareading;
        private System.Windows.Forms.Label modelbuilding;
        private System.Windows.Forms.Label finished;
        private Bunifu.Framework.UI.BunifuProgressBar bunifuProgressBar1;
        private Bunifu.Framework.UI.BunifuProgressBar bunifuProgressBar2;
        private Bunifu.Framework.UI.BunifuProgressBar bunifuProgressBar3;
        private System.Windows.Forms.PictureBox inittick;
        private System.Windows.Forms.PictureBox datareadtick;
        private System.Windows.Forms.PictureBox modelbuildtick;
        private System.Windows.Forms.PictureBox finishtick;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        //private System.Windows.Forms.Timer timer1;
    }
}