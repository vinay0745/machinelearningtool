﻿namespace MachineLearningTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Outputpanel = new System.Windows.Forms.Panel();
            this.Generate = new Bunifu.Framework.UI.BunifuThinButton2();
            this.confusionmatbox = new System.Windows.Forms.PictureBox();
            this.label18 = new System.Windows.Forms.Label();
            this.bunifuThinButton22 = new Bunifu.Framework.UI.BunifuThinButton2();
            this.panel1 = new System.Windows.Forms.Panel();
            this.traintime = new System.Windows.Forms.Label();
            this.consolepanel = new System.Windows.Forms.Panel();
            this.output = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LinearRegression = new Bunifu.Framework.UI.BunifuCheckbox();
            this.SGD = new Bunifu.Framework.UI.BunifuCheckbox();
            this.DTR = new Bunifu.Framework.UI.BunifuCheckbox();
            this.RFR = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.Perceptron = new Bunifu.Framework.UI.BunifuCheckbox();
            this.DTC = new Bunifu.Framework.UI.BunifuCheckbox();
            this.RFC = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.Algorithmpanel = new System.Windows.Forms.Panel();
            this.bunifuImageButton8 = new Bunifu.Framework.UI.BunifuImageButton();
            this.label21 = new System.Windows.Forms.Label();
            this.IC = new Bunifu.Framework.UI.BunifuCheckbox();
            this.label20 = new System.Windows.Forms.Label();
            this.bunifuImageButton7 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton6 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton5 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton4 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton3 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton2 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuImageButton1 = new Bunifu.Framework.UI.BunifuImageButton();
            this.bunifuSeparator5 = new Bunifu.Framework.UI.BunifuSeparator();
            this.bunifuSeparator4 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label17 = new System.Windows.Forms.Label();
            this.run = new Bunifu.Framework.UI.BunifuThinButton2();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.bunifuSlider1 = new Bunifu.Framework.UI.BunifuSlider();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.outputpanel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.bunifuCustomLabel2 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.Datapanel = new System.Windows.Forms.Panel();
            this.inputpanel1 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.Browsebutton = new Bunifu.Framework.UI.BunifuThinButton2();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip2 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip3 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip4 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip5 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip6 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip7 = new System.Windows.Forms.ToolTip(this.components);
            this.toolTip8 = new System.Windows.Forms.ToolTip(this.components);
            this.Outputpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.confusionmatbox)).BeginInit();
            this.panel1.SuspendLayout();
            this.consolepanel.SuspendLayout();
            this.Algorithmpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).BeginInit();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.Datapanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // Outputpanel
            // 
            this.Outputpanel.AutoSize = true;
            this.Outputpanel.BackColor = System.Drawing.Color.Gainsboro;
            this.Outputpanel.Controls.Add(this.Generate);
            this.Outputpanel.Controls.Add(this.confusionmatbox);
            this.Outputpanel.Controls.Add(this.label18);
            this.Outputpanel.Controls.Add(this.bunifuThinButton22);
            this.Outputpanel.Controls.Add(this.panel1);
            this.Outputpanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.Outputpanel.Location = new System.Drawing.Point(1272, 0);
            this.Outputpanel.Name = "Outputpanel";
            this.Outputpanel.Size = new System.Drawing.Size(652, 958);
            this.Outputpanel.TabIndex = 5;
            // 
            // Generate
            // 
            this.Generate.ActiveBorderThickness = 1;
            this.Generate.ActiveCornerRadius = 40;
            this.Generate.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.Generate.ActiveForecolor = System.Drawing.Color.White;
            this.Generate.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.Generate.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.Generate.BackColor = System.Drawing.Color.Gainsboro;
            this.Generate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Generate.BackgroundImage")));
            this.Generate.ButtonText = "Report";
            this.Generate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Generate.Font = new System.Drawing.Font("Segoe UI", 16.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Generate.ForeColor = System.Drawing.Color.Black;
            this.Generate.IdleBorderThickness = 2;
            this.Generate.IdleCornerRadius = 40;
            this.Generate.IdleFillColor = System.Drawing.Color.White;
            this.Generate.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.Generate.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.Generate.Location = new System.Drawing.Point(358, 814);
            this.Generate.Margin = new System.Windows.Forms.Padding(4);
            this.Generate.Name = "Generate";
            this.Generate.Size = new System.Drawing.Size(230, 78);
            this.Generate.TabIndex = 24;
            this.Generate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Generate.Click += new System.EventHandler(this.Generate_Click);
            // 
            // confusionmatbox
            // 
            this.confusionmatbox.Location = new System.Drawing.Point(67, 367);
            this.confusionmatbox.Name = "confusionmatbox";
            this.confusionmatbox.Size = new System.Drawing.Size(532, 418);
            this.confusionmatbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.confusionmatbox.TabIndex = 23;
            this.confusionmatbox.TabStop = false;
            this.confusionmatbox.Click += new System.EventHandler(this.confusionmatbox_Click);
            // 
            // label18
            // 
            this.label18.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(123, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(383, 66);
            this.label18.TabIndex = 22;
            this.label18.Text = "OUTPUT DISPLAY";
            this.label18.UseCompatibleTextRendering = true;
            // 
            // bunifuThinButton22
            // 
            this.bunifuThinButton22.ActiveBorderThickness = 1;
            this.bunifuThinButton22.ActiveCornerRadius = 40;
            this.bunifuThinButton22.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton22.ActiveForecolor = System.Drawing.Color.White;
            this.bunifuThinButton22.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton22.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bunifuThinButton22.BackColor = System.Drawing.Color.Gainsboro;
            this.bunifuThinButton22.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuThinButton22.BackgroundImage")));
            this.bunifuThinButton22.ButtonText = "Test";
            this.bunifuThinButton22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bunifuThinButton22.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuThinButton22.ForeColor = System.Drawing.Color.Black;
            this.bunifuThinButton22.IdleBorderThickness = 2;
            this.bunifuThinButton22.IdleCornerRadius = 40;
            this.bunifuThinButton22.IdleFillColor = System.Drawing.Color.White;
            this.bunifuThinButton22.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton22.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.bunifuThinButton22.Location = new System.Drawing.Point(98, 814);
            this.bunifuThinButton22.Margin = new System.Windows.Forms.Padding(6);
            this.bunifuThinButton22.Name = "bunifuThinButton22";
            this.bunifuThinButton22.Size = new System.Drawing.Size(230, 78);
            this.bunifuThinButton22.TabIndex = 20;
            this.bunifuThinButton22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuThinButton22.Click += new System.EventHandler(this.bunifuThinButton22_Click);
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel1.Controls.Add(this.traintime);
            this.panel1.Controls.Add(this.consolepanel);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Location = new System.Drawing.Point(67, 168);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(532, 167);
            this.panel1.TabIndex = 10;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // traintime
            // 
            this.traintime.AutoSize = true;
            this.traintime.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.traintime.Location = new System.Drawing.Point(155, 46);
            this.traintime.Name = "traintime";
            this.traintime.Size = new System.Drawing.Size(26, 30);
            this.traintime.TabIndex = 9;
            this.traintime.Text = "o";
            // 
            // consolepanel
            // 
            this.consolepanel.BackColor = System.Drawing.Color.LightSteelBlue;
            this.consolepanel.Controls.Add(this.output);
            this.consolepanel.Location = new System.Drawing.Point(152, 75);
            this.consolepanel.Name = "consolepanel";
            this.consolepanel.Size = new System.Drawing.Size(370, 89);
            this.consolepanel.TabIndex = 7;
            // 
            // output
            // 
            this.output.AutoSize = true;
            this.output.Font = new System.Drawing.Font("Segoe UI", 13.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.output.Location = new System.Drawing.Point(3, 5);
            this.output.Name = "output";
            this.output.Size = new System.Drawing.Size(26, 30);
            this.output.TabIndex = 8;
            this.output.Text = "o";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(19, 20);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(116, 38);
            this.label15.TabIndex = 4;
            this.label15.Text = "Console";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(853, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(187, 38);
            this.label6.TabIndex = 4;
            this.label6.Text = "REGRESSION";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(835, 438);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(237, 38);
            this.label7.TabIndex = 5;
            this.label7.Text = "CLASSIFICATION";
            // 
            // LinearRegression
            // 
            this.LinearRegression.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.LinearRegression.BackColor = System.Drawing.Color.White;
            this.LinearRegression.ChechedOffColor = System.Drawing.Color.White;
            this.LinearRegression.Checked = false;
            this.LinearRegression.CheckedOnColor = System.Drawing.Color.CornflowerBlue;
            this.LinearRegression.ForeColor = System.Drawing.Color.White;
            this.LinearRegression.Location = new System.Drawing.Point(759, 231);
            this.LinearRegression.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LinearRegression.Name = "LinearRegression";
            this.LinearRegression.Size = new System.Drawing.Size(20, 20);
            this.LinearRegression.TabIndex = 6;
            this.LinearRegression.TabStop = false;
            this.LinearRegression.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bunifuCheckbox1_MouseClick);
            // 
            // SGD
            // 
            this.SGD.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.SGD.BackColor = System.Drawing.Color.White;
            this.SGD.ChechedOffColor = System.Drawing.Color.White;
            this.SGD.Checked = false;
            this.SGD.CheckedOnColor = System.Drawing.Color.CornflowerBlue;
            this.SGD.ForeColor = System.Drawing.Color.White;
            this.SGD.Location = new System.Drawing.Point(759, 281);
            this.SGD.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.SGD.Name = "SGD";
            this.SGD.Size = new System.Drawing.Size(20, 20);
            this.SGD.TabIndex = 7;
            this.SGD.TabStop = false;
            this.SGD.OnChange += new System.EventHandler(this.bunifuCheckbox2_OnChange);
            this.SGD.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bunifuCheckbox2_MouseClick);
            // 
            // DTR
            // 
            this.DTR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.DTR.BackColor = System.Drawing.Color.White;
            this.DTR.ChechedOffColor = System.Drawing.Color.White;
            this.DTR.Checked = false;
            this.DTR.CheckedOnColor = System.Drawing.Color.CornflowerBlue;
            this.DTR.ForeColor = System.Drawing.Color.White;
            this.DTR.Location = new System.Drawing.Point(759, 328);
            this.DTR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DTR.Name = "DTR";
            this.DTR.Size = new System.Drawing.Size(20, 20);
            this.DTR.TabIndex = 8;
            this.DTR.TabStop = false;
            this.DTR.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bunifuCheckbox3_MouseClick);
            // 
            // RFR
            // 
            this.RFR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.RFR.BackColor = System.Drawing.Color.White;
            this.RFR.ChechedOffColor = System.Drawing.Color.White;
            this.RFR.Checked = false;
            this.RFR.CheckedOnColor = System.Drawing.Color.CornflowerBlue;
            this.RFR.ForeColor = System.Drawing.Color.White;
            this.RFR.Location = new System.Drawing.Point(759, 376);
            this.RFR.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RFR.Name = "RFR";
            this.RFR.Size = new System.Drawing.Size(20, 20);
            this.RFR.TabIndex = 9;
            this.RFR.TabStop = false;
            this.RFR.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bunifuCheckbox4_MouseClick);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(798, 222);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(292, 38);
            this.label8.TabIndex = 10;
            this.label8.Text = "Linear Regression OLS";
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(798, 272);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(297, 38);
            this.label9.TabIndex = 11;
            this.label9.Text = "Linear Regression SGD";
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(798, 319);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(314, 38);
            this.label10.TabIndex = 12;
            this.label10.Text = "Decision Tree Regressor";
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(798, 367);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(334, 38);
            this.label11.TabIndex = 13;
            this.label11.Text = "Random Forest Regressor";
            // 
            // Perceptron
            // 
            this.Perceptron.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Perceptron.BackColor = System.Drawing.Color.White;
            this.Perceptron.ChechedOffColor = System.Drawing.Color.White;
            this.Perceptron.Checked = false;
            this.Perceptron.CheckedOnColor = System.Drawing.Color.CornflowerBlue;
            this.Perceptron.ForeColor = System.Drawing.Color.White;
            this.Perceptron.Location = new System.Drawing.Point(769, 510);
            this.Perceptron.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Perceptron.Name = "Perceptron";
            this.Perceptron.Size = new System.Drawing.Size(20, 20);
            this.Perceptron.TabIndex = 14;
            this.Perceptron.TabStop = false;
            this.Perceptron.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bunifuCheckbox7_MouseClick);
            // 
            // DTC
            // 
            this.DTC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.DTC.BackColor = System.Drawing.Color.White;
            this.DTC.ChechedOffColor = System.Drawing.Color.White;
            this.DTC.Checked = false;
            this.DTC.CheckedOnColor = System.Drawing.Color.CornflowerBlue;
            this.DTC.ForeColor = System.Drawing.Color.White;
            this.DTC.Location = new System.Drawing.Point(769, 553);
            this.DTC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DTC.Name = "DTC";
            this.DTC.Size = new System.Drawing.Size(20, 20);
            this.DTC.TabIndex = 15;
            this.DTC.TabStop = false;
            this.DTC.OnChange += new System.EventHandler(this.bunifuCheckbox6_OnChange);
            this.DTC.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bunifuCheckbox6_MouseClick);
            // 
            // RFC
            // 
            this.RFC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.RFC.BackColor = System.Drawing.Color.White;
            this.RFC.ChechedOffColor = System.Drawing.Color.White;
            this.RFC.Checked = false;
            this.RFC.CheckedOnColor = System.Drawing.Color.CornflowerBlue;
            this.RFC.ForeColor = System.Drawing.Color.White;
            this.RFC.Location = new System.Drawing.Point(769, 596);
            this.RFC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.RFC.Name = "RFC";
            this.RFC.Size = new System.Drawing.Size(20, 20);
            this.RFC.TabIndex = 16;
            this.RFC.TabStop = false;
            this.RFC.OnChange += new System.EventHandler(this.bunifuCheckbox5_OnChange);
            this.RFC.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bunifuCheckbox5_MouseClick);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(808, 498);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(152, 38);
            this.label14.TabIndex = 17;
            this.label14.Text = "Perceptron";
            this.label14.Click += new System.EventHandler(this.label14_Click);
            // 
            // label13
            // 
            this.label13.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(808, 541);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(302, 38);
            this.label13.TabIndex = 18;
            this.label13.Text = "Decision Tree Classifier";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(808, 584);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(322, 38);
            this.label12.TabIndex = 19;
            this.label12.Text = "Random Forest Classifier";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // Algorithmpanel
            // 
            this.Algorithmpanel.BackColor = System.Drawing.Color.Gainsboro;
            this.Algorithmpanel.Controls.Add(this.bunifuImageButton8);
            this.Algorithmpanel.Controls.Add(this.label21);
            this.Algorithmpanel.Controls.Add(this.IC);
            this.Algorithmpanel.Controls.Add(this.label20);
            this.Algorithmpanel.Controls.Add(this.bunifuImageButton7);
            this.Algorithmpanel.Controls.Add(this.bunifuImageButton6);
            this.Algorithmpanel.Controls.Add(this.bunifuImageButton5);
            this.Algorithmpanel.Controls.Add(this.bunifuImageButton4);
            this.Algorithmpanel.Controls.Add(this.bunifuImageButton3);
            this.Algorithmpanel.Controls.Add(this.bunifuImageButton2);
            this.Algorithmpanel.Controls.Add(this.bunifuImageButton1);
            this.Algorithmpanel.Controls.Add(this.bunifuSeparator5);
            this.Algorithmpanel.Controls.Add(this.bunifuSeparator4);
            this.Algorithmpanel.Controls.Add(this.label17);
            this.Algorithmpanel.Controls.Add(this.run);
            this.Algorithmpanel.Controls.Add(this.label12);
            this.Algorithmpanel.Controls.Add(this.label13);
            this.Algorithmpanel.Controls.Add(this.label14);
            this.Algorithmpanel.Controls.Add(this.RFC);
            this.Algorithmpanel.Controls.Add(this.DTC);
            this.Algorithmpanel.Controls.Add(this.Perceptron);
            this.Algorithmpanel.Controls.Add(this.label11);
            this.Algorithmpanel.Controls.Add(this.label10);
            this.Algorithmpanel.Controls.Add(this.label9);
            this.Algorithmpanel.Controls.Add(this.label8);
            this.Algorithmpanel.Controls.Add(this.RFR);
            this.Algorithmpanel.Controls.Add(this.DTR);
            this.Algorithmpanel.Controls.Add(this.SGD);
            this.Algorithmpanel.Controls.Add(this.LinearRegression);
            this.Algorithmpanel.Controls.Add(this.label7);
            this.Algorithmpanel.Controls.Add(this.label6);
            this.Algorithmpanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Algorithmpanel.Location = new System.Drawing.Point(0, 0);
            this.Algorithmpanel.Name = "Algorithmpanel";
            this.Algorithmpanel.Size = new System.Drawing.Size(1924, 958);
            this.Algorithmpanel.TabIndex = 4;
            this.Algorithmpanel.Paint += new System.Windows.Forms.PaintEventHandler(this.Algorithmpanel_Paint);
            // 
            // bunifuImageButton8
            // 
            this.bunifuImageButton8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuImageButton8.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton8.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton8.Image")));
            this.bunifuImageButton8.ImageActive = null;
            this.bunifuImageButton8.Location = new System.Drawing.Point(1144, 718);
            this.bunifuImageButton8.Name = "bunifuImageButton8";
            this.bunifuImageButton8.Size = new System.Drawing.Size(38, 29);
            this.bunifuImageButton8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton8.TabIndex = 38;
            this.bunifuImageButton8.TabStop = false;
            this.toolTip8.SetToolTip(this.bunifuImageButton8, "Image Classifier");
            this.bunifuImageButton8.Zoom = 10;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(808, 718);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(211, 38);
            this.label21.TabIndex = 37;
            this.label21.Text = "Image Classifier";
            // 
            // IC
            // 
            this.IC.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.IC.BackColor = System.Drawing.Color.White;
            this.IC.ChechedOffColor = System.Drawing.Color.White;
            this.IC.Checked = false;
            this.IC.CheckedOnColor = System.Drawing.Color.CornflowerBlue;
            this.IC.ForeColor = System.Drawing.Color.White;
            this.IC.Location = new System.Drawing.Point(769, 730);
            this.IC.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IC.Name = "IC";
            this.IC.Size = new System.Drawing.Size(20, 20);
            this.IC.TabIndex = 36;
            this.IC.TabStop = false;
            this.IC.OnChange += new System.EventHandler(this.IC_OnChange);
            this.IC.MouseClick += new System.Windows.Forms.MouseEventHandler(this.IC_MouseClick);
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(803, 655);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(312, 38);
            this.label20.TabIndex = 35;
            this.label20.Text = "VISUAL RECOGNITION";
            // 
            // bunifuImageButton7
            // 
            this.bunifuImageButton7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuImageButton7.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton7.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton7.Image")));
            this.bunifuImageButton7.ImageActive = null;
            this.bunifuImageButton7.Location = new System.Drawing.Point(1144, 593);
            this.bunifuImageButton7.Name = "bunifuImageButton7";
            this.bunifuImageButton7.Size = new System.Drawing.Size(38, 29);
            this.bunifuImageButton7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton7.TabIndex = 34;
            this.bunifuImageButton7.TabStop = false;
            this.toolTip7.SetToolTip(this.bunifuImageButton7, "Random Forest Classifier");
            this.bunifuImageButton7.Zoom = 10;
            // 
            // bunifuImageButton6
            // 
            this.bunifuImageButton6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuImageButton6.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton6.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton6.Image")));
            this.bunifuImageButton6.ImageActive = null;
            this.bunifuImageButton6.Location = new System.Drawing.Point(1144, 550);
            this.bunifuImageButton6.Name = "bunifuImageButton6";
            this.bunifuImageButton6.Size = new System.Drawing.Size(38, 29);
            this.bunifuImageButton6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton6.TabIndex = 33;
            this.bunifuImageButton6.TabStop = false;
            this.toolTip6.SetToolTip(this.bunifuImageButton6, "Decision Tree Classifier");
            this.bunifuImageButton6.Zoom = 10;
            // 
            // bunifuImageButton5
            // 
            this.bunifuImageButton5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuImageButton5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton5.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton5.Image")));
            this.bunifuImageButton5.ImageActive = null;
            this.bunifuImageButton5.Location = new System.Drawing.Point(1144, 507);
            this.bunifuImageButton5.Name = "bunifuImageButton5";
            this.bunifuImageButton5.Size = new System.Drawing.Size(38, 29);
            this.bunifuImageButton5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton5.TabIndex = 32;
            this.bunifuImageButton5.TabStop = false;
            this.toolTip5.SetToolTip(this.bunifuImageButton5, "Perceptron");
            this.bunifuImageButton5.Zoom = 10;
            // 
            // bunifuImageButton4
            // 
            this.bunifuImageButton4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuImageButton4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton4.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton4.Image")));
            this.bunifuImageButton4.ImageActive = null;
            this.bunifuImageButton4.Location = new System.Drawing.Point(1144, 374);
            this.bunifuImageButton4.Name = "bunifuImageButton4";
            this.bunifuImageButton4.Size = new System.Drawing.Size(38, 29);
            this.bunifuImageButton4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton4.TabIndex = 31;
            this.bunifuImageButton4.TabStop = false;
            this.toolTip4.SetToolTip(this.bunifuImageButton4, "Random Forest Regressor");
            this.bunifuImageButton4.Zoom = 10;
            // 
            // bunifuImageButton3
            // 
            this.bunifuImageButton3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuImageButton3.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton3.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton3.Image")));
            this.bunifuImageButton3.ImageActive = null;
            this.bunifuImageButton3.Location = new System.Drawing.Point(1144, 319);
            this.bunifuImageButton3.Name = "bunifuImageButton3";
            this.bunifuImageButton3.Size = new System.Drawing.Size(38, 29);
            this.bunifuImageButton3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton3.TabIndex = 30;
            this.bunifuImageButton3.TabStop = false;
            this.toolTip3.SetToolTip(this.bunifuImageButton3, "Decision Tree Regressor");
            this.bunifuImageButton3.Zoom = 10;
            // 
            // bunifuImageButton2
            // 
            this.bunifuImageButton2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuImageButton2.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton2.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton2.Image")));
            this.bunifuImageButton2.ImageActive = null;
            this.bunifuImageButton2.Location = new System.Drawing.Point(1144, 274);
            this.bunifuImageButton2.Name = "bunifuImageButton2";
            this.bunifuImageButton2.Size = new System.Drawing.Size(38, 29);
            this.bunifuImageButton2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton2.TabIndex = 29;
            this.bunifuImageButton2.TabStop = false;
            this.toolTip2.SetToolTip(this.bunifuImageButton2, "Linear Regression (SGD)");
            this.bunifuImageButton2.Zoom = 10;
            // 
            // bunifuImageButton1
            // 
            this.bunifuImageButton1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuImageButton1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuImageButton1.Image = ((System.Drawing.Image)(resources.GetObject("bunifuImageButton1.Image")));
            this.bunifuImageButton1.ImageActive = null;
            this.bunifuImageButton1.Location = new System.Drawing.Point(1144, 231);
            this.bunifuImageButton1.Name = "bunifuImageButton1";
            this.bunifuImageButton1.Size = new System.Drawing.Size(38, 29);
            this.bunifuImageButton1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bunifuImageButton1.TabIndex = 28;
            this.bunifuImageButton1.TabStop = false;
            this.toolTip1.SetToolTip(this.bunifuImageButton1, "Linear Regression (OLS)");
            this.bunifuImageButton1.Zoom = 10;
            // 
            // bunifuSeparator5
            // 
            this.bunifuSeparator5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuSeparator5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator5.LineThickness = 1;
            this.bunifuSeparator5.Location = new System.Drawing.Point(1255, 78);
            this.bunifuSeparator5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuSeparator5.Name = "bunifuSeparator5";
            this.bunifuSeparator5.Size = new System.Drawing.Size(10, 794);
            this.bunifuSeparator5.TabIndex = 27;
            this.bunifuSeparator5.Transparency = 255;
            this.bunifuSeparator5.Vertical = true;
            // 
            // bunifuSeparator4
            // 
            this.bunifuSeparator4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.bunifuSeparator4.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSeparator4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(105)))), ((int)(((byte)(105)))), ((int)(((byte)(105)))));
            this.bunifuSeparator4.LineThickness = 1;
            this.bunifuSeparator4.Location = new System.Drawing.Point(650, 78);
            this.bunifuSeparator4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuSeparator4.Name = "bunifuSeparator4";
            this.bunifuSeparator4.Size = new System.Drawing.Size(10, 794);
            this.bunifuSeparator4.TabIndex = 26;
            this.bunifuSeparator4.Transparency = 255;
            this.bunifuSeparator4.Vertical = true;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(814, 40);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(272, 66);
            this.label17.TabIndex = 22;
            this.label17.Text = "ML MODELS";
            this.label17.UseCompatibleTextRendering = true;
            // 
            // run
            // 
            this.run.ActiveBorderThickness = 1;
            this.run.ActiveCornerRadius = 40;
            this.run.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.run.ActiveForecolor = System.Drawing.Color.White;
            this.run.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.run.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.run.BackColor = System.Drawing.Color.Gainsboro;
            this.run.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("run.BackgroundImage")));
            this.run.ButtonText = "Run";
            this.run.Cursor = System.Windows.Forms.Cursors.Hand;
            this.run.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.run.ForeColor = System.Drawing.Color.Black;
            this.run.IdleBorderThickness = 2;
            this.run.IdleCornerRadius = 40;
            this.run.IdleFillColor = System.Drawing.Color.White;
            this.run.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.run.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.run.Location = new System.Drawing.Point(829, 814);
            this.run.Margin = new System.Windows.Forms.Padding(6, 5, 6, 5);
            this.run.Name = "run";
            this.run.Size = new System.Drawing.Size(264, 78);
            this.run.TabIndex = 0;
            this.run.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.run.Click += new System.EventHandler(this.run_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(82, 167);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 43);
            this.label1.TabIndex = 1;
            this.label1.Text = "Data";
            this.label1.UseCompatibleTextRendering = true;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(90, 829);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 38);
            this.label4.TabIndex = 7;
            this.label4.Text = "Split";
            // 
            // bunifuSlider1
            // 
            this.bunifuSlider1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bunifuSlider1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuSlider1.BackgroudColor = System.Drawing.Color.DarkGray;
            this.bunifuSlider1.BorderRadius = 0;
            this.bunifuSlider1.IndicatorColor = System.Drawing.Color.CornflowerBlue;
            this.bunifuSlider1.Location = new System.Drawing.Point(188, 837);
            this.bunifuSlider1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bunifuSlider1.MaximumValue = 100;
            this.bunifuSlider1.Name = "bunifuSlider1";
            this.bunifuSlider1.Size = new System.Drawing.Size(355, 35);
            this.bunifuSlider1.TabIndex = 8;
            this.bunifuSlider1.TabStop = false;
            this.bunifuSlider1.Value = 20;
            this.bunifuSlider1.ValueChanged += new System.EventHandler(this.bunifuSlider1_ValueChanged);
            this.bunifuSlider1.ValueChangeComplete += new System.EventHandler(this.bunifuSlider1_ValueChangeComplete);
            this.bunifuSlider1.DragOver += new System.Windows.Forms.DragEventHandler(this.bunifuSlider1_DragOver);
            this.bunifuSlider1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bunifuSlider1_MouseClick);
            this.bunifuSlider1.MouseEnter += new System.EventHandler(this.bunifuSlider1_MouseEnter);
            // 
            // panel4
            // 
            this.panel4.AutoSize = true;
            this.panel4.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Location = new System.Drawing.Point(75, 258);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(524, 245);
            this.panel4.TabIndex = 9;
            this.panel4.Paint += new System.Windows.Forms.PaintEventHandler(this.panel4_Paint);
            this.panel4.Resize += new System.EventHandler(this.panel4_Resize);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 38);
            this.label2.TabIndex = 3;
            this.label2.Text = "Inputs";
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.panel5.AutoSize = true;
            this.panel5.BackColor = System.Drawing.Color.LightSteelBlue;
            this.panel5.Controls.Add(this.outputpanel1);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Location = new System.Drawing.Point(79, 518);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(521, 248);
            this.panel5.TabIndex = 10;
            this.panel5.Paint += new System.Windows.Forms.PaintEventHandler(this.panel5_Paint);
            // 
            // outputpanel1
            // 
            this.outputpanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.outputpanel1.Location = new System.Drawing.Point(135, 26);
            this.outputpanel1.Name = "outputpanel1";
            this.outputpanel1.Size = new System.Drawing.Size(383, 204);
            this.outputpanel1.TabIndex = 5;
            this.outputpanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.outputpanel1_Paint);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 38);
            this.label3.TabIndex = 4;
            this.label3.Text = "Output";
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(182, 863);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(43, 23);
            this.bunifuCustomLabel1.TabIndex = 11;
            this.bunifuCustomLabel1.Text = "Test:";
            // 
            // bunifuCustomLabel2
            // 
            this.bunifuCustomLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bunifuCustomLabel2.AutoSize = true;
            this.bunifuCustomLabel2.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel2.Location = new System.Drawing.Point(470, 863);
            this.bunifuCustomLabel2.Name = "bunifuCustomLabel2";
            this.bunifuCustomLabel2.Size = new System.Drawing.Size(51, 23);
            this.bunifuCustomLabel2.TabIndex = 12;
            this.bunifuCustomLabel2.Text = "Train:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(223, 865);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(27, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "20";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Segoe UI", 25.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(206, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(327, 66);
            this.label16.TabIndex = 21;
            this.label16.Text = "DATA UPLOAD";
            this.label16.UseCompatibleTextRendering = true;
            // 
            // Datapanel
            // 
            this.Datapanel.BackColor = System.Drawing.Color.Gainsboro;
            this.Datapanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Datapanel.Controls.Add(this.inputpanel1);
            this.Datapanel.Controls.Add(this.label19);
            this.Datapanel.Controls.Add(this.label16);
            this.Datapanel.Controls.Add(this.Browsebutton);
            this.Datapanel.Controls.Add(this.label5);
            this.Datapanel.Controls.Add(this.bunifuCustomLabel2);
            this.Datapanel.Controls.Add(this.bunifuCustomLabel1);
            this.Datapanel.Controls.Add(this.panel5);
            this.Datapanel.Controls.Add(this.panel4);
            this.Datapanel.Controls.Add(this.bunifuSlider1);
            this.Datapanel.Controls.Add(this.label4);
            this.Datapanel.Controls.Add(this.label1);
            this.Datapanel.Cursor = System.Windows.Forms.Cursors.Default;
            this.Datapanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.Datapanel.Location = new System.Drawing.Point(0, 0);
            this.Datapanel.Name = "Datapanel";
            this.Datapanel.Size = new System.Drawing.Size(643, 958);
            this.Datapanel.TabIndex = 5;
            // 
            // inputpanel1
            // 
            this.inputpanel1.BackColor = System.Drawing.Color.LightSteelBlue;
            this.inputpanel1.Location = new System.Drawing.Point(210, 274);
            this.inputpanel1.Name = "inputpanel1";
            this.inputpanel1.Size = new System.Drawing.Size(386, 217);
            this.inputpanel1.TabIndex = 6;
            this.inputpanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.inputpanel1_Paint);
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(518, 864);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(27, 20);
            this.label19.TabIndex = 22;
            this.label19.Text = "80";
            // 
            // Browsebutton
            // 
            this.Browsebutton.ActiveBorderThickness = 1;
            this.Browsebutton.ActiveCornerRadius = 20;
            this.Browsebutton.ActiveFillColor = System.Drawing.Color.DodgerBlue;
            this.Browsebutton.ActiveForecolor = System.Drawing.Color.White;
            this.Browsebutton.ActiveLineColor = System.Drawing.Color.DodgerBlue;
            this.Browsebutton.BackColor = System.Drawing.Color.Gainsboro;
            this.Browsebutton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Browsebutton.BackgroundImage")));
            this.Browsebutton.ButtonText = "Select File";
            this.Browsebutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Browsebutton.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Browsebutton.ForeColor = System.Drawing.Color.Black;
            this.Browsebutton.IdleBorderThickness = 1;
            this.Browsebutton.IdleCornerRadius = 20;
            this.Browsebutton.IdleFillColor = System.Drawing.Color.White;
            this.Browsebutton.IdleForecolor = System.Drawing.Color.DodgerBlue;
            this.Browsebutton.IdleLineColor = System.Drawing.Color.DodgerBlue;
            this.Browsebutton.Location = new System.Drawing.Point(156, 159);
            this.Browsebutton.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Browsebutton.Name = "Browsebutton";
            this.Browsebutton.Size = new System.Drawing.Size(431, 58);
            this.Browsebutton.TabIndex = 20;
            this.Browsebutton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Browsebutton.Click += new System.EventHandler(this.Browsebutton_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.BackColor = System.Drawing.Color.WhiteSmoke;
            // 
            // toolTip2
            // 
            this.toolTip2.BackColor = System.Drawing.Color.WhiteSmoke;
            // 
            // toolTip3
            // 
            this.toolTip3.BackColor = System.Drawing.Color.WhiteSmoke;
            // 
            // toolTip4
            // 
            this.toolTip4.BackColor = System.Drawing.Color.WhiteSmoke;
            // 
            // toolTip5
            // 
            this.toolTip5.BackColor = System.Drawing.Color.WhiteSmoke;
            // 
            // toolTip6
            // 
            this.toolTip6.BackColor = System.Drawing.Color.WhiteSmoke;
            // 
            // toolTip7
            // 
            this.toolTip7.BackColor = System.Drawing.Color.WhiteSmoke;
            // 
            // toolTip8
            // 
            this.toolTip8.BackColor = System.Drawing.Color.WhiteSmoke;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(1924, 958);
            this.Controls.Add(this.Outputpanel);
            this.Controls.Add(this.Datapanel);
            this.Controls.Add(this.Algorithmpanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Machine Learning Tool";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Outputpanel.ResumeLayout(false);
            this.Outputpanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.confusionmatbox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.consolepanel.ResumeLayout(false);
            this.consolepanel.PerformLayout();
            this.Algorithmpanel.ResumeLayout(false);
            this.Algorithmpanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bunifuImageButton1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.Datapanel.ResumeLayout(false);
            this.Datapanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel Outputpanel;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuThinButton2 bunifuThinButton22;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuCheckbox LinearRegression;
        private Bunifu.Framework.UI.BunifuCheckbox SGD;
        private Bunifu.Framework.UI.BunifuCheckbox DTR;
        private Bunifu.Framework.UI.BunifuCheckbox RFR;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuCheckbox Perceptron;
        private Bunifu.Framework.UI.BunifuCheckbox DTC;
        private Bunifu.Framework.UI.BunifuCheckbox RFC;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private Bunifu.Framework.UI.BunifuThinButton2 run;
        private System.Windows.Forms.Panel Algorithmpanel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private Bunifu.Framework.UI.BunifuSlider bunifuSlider1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel2;
        public System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuThinButton2 Browsebutton;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel Datapanel;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator4;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel outputpanel1;
        private System.Windows.Forms.Panel inputpanel1;
        private System.Windows.Forms.Panel consolepanel;
        private System.Windows.Forms.Label output;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton1;
        private System.Windows.Forms.ToolTip toolTip1;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton2;
        private System.Windows.Forms.ToolTip toolTip2;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton7;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton6;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton5;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton4;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton3;
        private System.Windows.Forms.ToolTip toolTip7;
        private System.Windows.Forms.ToolTip toolTip6;
        private System.Windows.Forms.ToolTip toolTip5;
        private System.Windows.Forms.ToolTip toolTip4;
        private System.Windows.Forms.ToolTip toolTip3;
        private System.Windows.Forms.Label traintime;
        private Bunifu.Framework.UI.BunifuImageButton bunifuImageButton8;
        private System.Windows.Forms.Label label21;
        private Bunifu.Framework.UI.BunifuCheckbox IC;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ToolTip toolTip8;
        private System.Windows.Forms.PictureBox confusionmatbox;
        private Bunifu.Framework.UI.BunifuThinButton2 Generate;
    }
}

